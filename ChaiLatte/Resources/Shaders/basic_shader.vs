#version 330

in vec3 vertexPosition;
in vec2 texCoords;
in vec3 vertexNormal;
in vec3 tangent;

out vec2 textureCoordinates;
out vec3 fragVert;
out mat3 tbnMatrix;

uniform mat4 model;
uniform mat4 viewProjection;

void main()
{
    textureCoordinates = vec2(texCoords.x, 1 - texCoords.y);
    gl_Position = viewProjection * model * vec4(vertexPosition, 1.0);
    fragVert = (model * vec4(vertexPosition, 1.0)).xyz;

    vec3 n = normalize((model * vec4(vertexNormal, 0.0)).xyz);
    vec3 t = normalize((model * vec4(tangent, 0.0)).xyz);

    // Reorthogonalize using Graham-Schmidt process
    t = t - dot(t, n) * n;
    
    vec3 b = normalize(cross(n, t));
    
    tbnMatrix = mat3(t, b, n);
}