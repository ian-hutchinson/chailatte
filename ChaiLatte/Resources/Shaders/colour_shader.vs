#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec4 vertexColour;

uniform mat4 model;
uniform mat4 viewProjection;

out vec4 vertColour;

void main()
{
    gl_Position = viewProjection * model * vec4(vertexPosition, 1.0);
    vertColour = vertexColour;
}