#version 330

in vec2 textureCoordinates;
in vec3 fragVert;
in mat3 tbnMatrix;

uniform mat4 model;
uniform vec3 cameraPosition;

// material
uniform sampler2D materialTexture;
uniform float materialShininess;
uniform vec3 materialSpecularColour;

uniform sampler2D normalTexture;

uniform struct Light
{
    vec3 position;
    vec3 intensities;
    float ambientCoefficient;
    float attenuation;
} light;

out vec4 fragColour;


void main()
{
    vec3 normal = normalize(tbnMatrix * (2 * texture(normalTexture, textureCoordinates).xyz - 1));

    vec3 surfacePos = vec3(model * vec4(fragVert, 1));
    vec4 surfaceColour = texture(materialTexture, textureCoordinates);
    vec3 surfaceToLight = normalize(light.position - surfacePos);
    vec3 surfaceToCamera = normalize(cameraPosition - surfacePos);

    // ambient
    vec3 ambient = light.ambientCoefficient * surfaceColour.rgb * light.intensities;
    
    // diffuse
    float diffuseCoefficient = max(0.0, dot(normal, -surfaceToLight));
    vec3 diffuse = diffuseCoefficient * surfaceColour.rgb * light.intensities;
    
    // specular
    float specularCoefficient = 0.0;
    
    if (diffuseCoefficient > 0.0)
    {
        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), materialShininess);
    }

    vec3 specular = specularCoefficient * materialSpecularColour * light.intensities;
    
    // attenuation
    float distanceToLight = length(light.position - surfacePos);
    float attenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));
    
    vec3 linearColour = ambient + attenuation * (diffuse + specular);
    
    // gamma correction
    // vec3 gamma = vec3(1.0/2.2);
    fragColour = vec4(linearColour, surfaceColour.a);

    //fragColour = vec4(1.0, 0.0, 0.0, 1.0);
}
