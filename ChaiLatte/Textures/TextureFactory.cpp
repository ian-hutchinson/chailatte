//
//  TextureFactory.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 19/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "TextureFactory.h"
#include "Texture.h"
#include "IImageLoader.h"
#include "Image.h"

namespace ChaiLatte
{
    namespace Textures
    {
        TextureFactory::TextureFactory(Images::IImageLoader& imageLoader)
        : m_textureSearchPath("../../ChaiLatte/Resources/Textures/")
        , m_imageLoader(imageLoader)
        {
            
        }
        
        Texture* TextureFactory::CreateTexture(const std::string& textureName)
        {
            std::string fullTexturePath = m_textureSearchPath + textureName;
            
            Images::Image* pImage = m_imageLoader.LoadImage(fullTexturePath);
            
            GLuint textureId;
            glGenTextures(1, &textureId);
            
            Texture* pTexture = new Texture(textureId, pImage->GetWidth(), pImage->GetHeight(), textureName);
            
            pTexture->Bind(0);
            
            // Todo factor out some texture options, wrapping, pixel format etc
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            
            glTexImage2D(GL_TEXTURE_2D,
                         0,
                         GL_RGB,
                         pImage->GetWidth(),
                         pImage->GetHeight(),
                         0,
                         GL_RGB,
                         GL_UNSIGNED_BYTE,
                         pImage->GetBuffer());
 
            pTexture->Unbind();

            delete pImage;
            
            return pTexture;
        }
    }
}