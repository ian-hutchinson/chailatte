//
//  TextureRepository.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 04/03/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__TextureRepository__
#define __ChaiLatte__TextureRepository__

#include "Textures.h"

#include <map>
#include <string>

namespace ChaiLatte
{
    namespace Textures
    {
        typedef std::pair<std::string, ITexture*> TTexturePair;
        
        class TextureRepository
        {
        public:
            TextureRepository();
            ~TextureRepository();
            
            void AddTexture(ITexture* pTexture);
            void RemoveTexture(const std::string& textureName);
            ITexture& GetTexture(const std::string& textureName) const;
            size_t Size() const;
            
        private:
            typedef std::map<std::string, ITexture*> TTextureMap;
            TTextureMap m_textureMap;
        };
    }
}


#endif /* defined(__ChaiLatte__TextureRepository__) */
