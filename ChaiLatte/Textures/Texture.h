//
//  Texture.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 19/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Texture_h
#define ChaiLatte_Texture_h

#include "ITexture.h"

namespace ChaiLatte
{
    namespace Textures
    {
        class Texture : public ITexture
        {
        public:
            Texture(TTextureId id, int width, int height, const std::string& name);
            
            TTextureId GetId() const;
            unsigned int GetWidth() const;
            unsigned int GetHeight() const;
            const std::string& GetName() const;
            void Bind(unsigned int textureUnit);
            void Unbind();
            
        private:
            TTextureId m_id;
            unsigned int m_width;
            unsigned int m_height;
            std::string m_name;
            unsigned int m_bindedUnit;
        };
    }
}

#endif
