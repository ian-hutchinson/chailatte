//
//  TextureFactory.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 19/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_TextureFactory_h
#define ChaiLatte_TextureFactory_h

#include "Textures.h"
#include "Images.h"

#include <string>

namespace ChaiLatte
{
    namespace Textures
    {
        class TextureFactory
        {
        public:
            TextureFactory(Images::IImageLoader& imageLoader);
            
            Texture* CreateTexture(const std::string& textureName);
            
        private:
            std::string m_textureSearchPath;
            
            Images::IImageLoader& m_imageLoader;
        };
    }
}

#endif
