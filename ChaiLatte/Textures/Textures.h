//
//  Textures.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 19/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Textures_h
#define ChaiLatte_Textures_h

namespace ChaiLatte
{
    namespace Textures
    {
        class ITexture;
        class Texture;
        class TextureRepository;
        class TextureFactory;
    }
}
#endif
