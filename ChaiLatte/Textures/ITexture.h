//
//  ITexture.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 19/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_ITexture_h
#define ChaiLatte_ITexture_h

#include <GL/glew.h>
#include <string>

namespace ChaiLatte
{
    namespace Textures
    {
        typedef GLint TTextureId;
        
        class ITexture
        {
        public:
            virtual ~ITexture() { }
            virtual TTextureId GetId() const = 0;
            virtual unsigned int GetWidth() const = 0;
            virtual unsigned int GetHeight() const = 0;
            virtual const std::string& GetName() const = 0;
            virtual void Bind(unsigned int textureUnit) = 0;
            virtual void Unbind() = 0;
        };
    }
}

#endif
