//
//  Texture.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 19/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "Texture.h"

namespace ChaiLatte
{
    namespace Textures
    {
        Texture::Texture(TTextureId id, int width, int height, const std::string& name)
        : m_id(id)
        , m_width(width)
        , m_height(height)
        , m_name(name)
        , m_bindedUnit(0)
        {
            
        }
        
        TTextureId Texture::GetId() const
        {
            return m_id;
        }
        
        unsigned int Texture::GetWidth() const
        {
            return m_width;
        }
        
        unsigned int Texture::GetHeight() const
        {
            return m_height;
        }
        
        const std::string& Texture::GetName() const
        {
            return m_name;
        }
        
        void Texture::Bind(unsigned int textureUnit)
        {
            m_bindedUnit = textureUnit;
            glActiveTexture(GL_TEXTURE0 + m_bindedUnit);
            glBindTexture(GL_TEXTURE_2D, m_id);
        }
        
        void Texture::Unbind()
        {
            glActiveTexture(GL_TEXTURE0 + m_bindedUnit);
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }
}
