//
//  TextureInfo.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 24/02/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_TextureInfo_h
#define ChaiLatte_TextureInfo_h

#include <string>

#include <GL/glew.h>

namespace ChaiLatte
{
    namespace Textures
    {
        struct TextureInfo
        {
            TextureInfo(bool gammaCorrected, const std::string& filename)
            : gammaCorrected(gammaCorrected)
            , filename(filename)
            {
                
            }
            
      
            std::string filename;
            bool gammaCorrected;

        };
    }
}

#endif
