//
//  TextureRepository.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 04/03/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "TextureRepository.h"
#include "ITexture.h"

#include <iostream> // Sort of temp, need a better logger...
#include <cassert>

namespace ChaiLatte
{
    namespace Textures
    {
        TextureRepository::TextureRepository()
        {
            
        }
        
        TextureRepository::~TextureRepository()
        {
            for (auto& texturePair : m_textureMap)
            {
                delete texturePair.second;
            }
            
            m_textureMap.clear();
        }
        
        void TextureRepository::AddTexture(ITexture* pTexture)
        {
            const auto& textureName = pTexture->GetName();
            
            if (m_textureMap.find(textureName) != m_textureMap.end())
            {
                std::cerr << "Couldn't add texture " + textureName + ", already exists\n";
                return;
            }
            
            m_textureMap.insert(TTexturePair(textureName, pTexture));
        }
        
        void TextureRepository::RemoveTexture(const std::string &textureName)
        {
            TTextureMap::iterator it = m_textureMap.find(textureName);
            assert(it != m_textureMap.end());
            
            delete it->second;
            m_textureMap.erase(it);
        }
        
        ITexture& TextureRepository::GetTexture(const std::string& textureName) const
        {
            TTextureMap::const_iterator it = m_textureMap.find(textureName);
            assert(it != m_textureMap.end());
            
            return *it->second;
        }
        
        size_t TextureRepository::Size() const
        {
            return m_textureMap.size();
        }
    }
}