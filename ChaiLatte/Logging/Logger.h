//
//  Logger.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 20/02/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__Logger__
#define __ChaiLatte__Logger__

#include <ostream>
#include <fstream>
#include <string>
#include <cassert>

#define __CONTEXT__ __FILE__,__LINE__
#define CHAI_LOG(message) ChaiLatte::Logging::Logger::Log(__CONTEXT__, message)

namespace ChaiLatte
{
    namespace Logging
    {
        class Logger
        {
        private:
            static std::ofstream m_outputFile;
            static std::ostream* m_pStream;
            static bool m_writeToFile;
        public:
            static void InitialiseLogFile(const std::string& filename);
            static void Teardown();
            static void Log(std::string file, int line, std::string message);
        };
    }
}
#endif /* defined(__ChaiLatte__Logger__) */
