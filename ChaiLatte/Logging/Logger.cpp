//
//  Logger.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 20/02/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "Logger.h"
#include <iostream>
#include <ctime>

#define LOGDATE(x) x.substr(0, x.length()-1)
namespace ChaiLatte
{
    namespace Logging
    {
        std::ofstream Logger::m_outputFile;
        std::ostream* Logger::m_pStream = nullptr;
        bool Logger::m_writeToFile = false;

        void Logger::InitialiseLogFile(const std::string& filename)
        {
            if (m_pStream)
            {
                return;
            }

            if (!filename.empty())
            {
                m_outputFile.open(filename);
                assert(m_outputFile);
                m_pStream = &m_outputFile;
                m_writeToFile = true;
            }
            else
            {
                m_pStream = &std::cerr;
            }
            
            time_t now = time(NULL);
            char *date = ctime(&now);
            std::string date_str(date);
            
            *m_pStream << LOGDATE(date_str) << ": Logger started" << std::endl;
        }
        
        void Logger::Teardown()
        {
            time_t now = time(NULL);
            char *date = ctime(&now);
            std::string date_str(date);
            
            *m_pStream << LOGDATE(date_str) << ": Logger terminated" << std::endl;
            m_pStream = nullptr;

            if (m_writeToFile)
            {
                m_outputFile.close();
            }
        }
        
        void Logger::Log(std::string file, int line, std::string message)
        {
            if (!m_pStream)
            {
                InitialiseLogFile("");
            }

            time_t now = time(NULL);
            char *date = ctime(&now);
            std::string date_str(date);
            file = file.substr(file.find_last_of('/')+1 );
            
            *m_pStream << LOGDATE(date_str) << " in " << file << "[line: " << line << "]: ";
            *m_pStream << message << std::endl;
        }
    }
}