//
// Created by Ian Hutchinson on 27/07/15.
//

#ifndef CHAILATTE_SCENEBASE_H
#define CHAILATTE_SCENEBASE_H

#include "IScene.h"
#include <string>

namespace ChaiLatte
{
    namespace Scenes
    {
        class SceneBase : public IScene
        {
        public:
            SceneBase(const std::string& sceneName);

            const std::string& GetName() const;
        private:
            std::string m_name;
        };
    }
}



#endif //CHAILATTE_SCENEBASE_H
