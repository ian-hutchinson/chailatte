//
// Created by Ian Hutchinson on 27/07/15.
//

#ifndef CHAILATTE_SCENESFWD_H
#define CHAILATTE_SCENESFWD_H

namespace ChaiLatte
{
    namespace Scenes
    {
        class IScene;
        class ISceneManager;
        class ISceneFactory;
        class ISceneController;
        class SceneController;
    }
}

#endif //CHAILATTE_SCENESFWD_H
