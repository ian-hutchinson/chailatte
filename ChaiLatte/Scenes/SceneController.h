//
// Created by Ian Hutchinson on 09/08/15.
//

#ifndef CHAILATTE_SCENECONTROLLER_H
#define CHAILATTE_SCENECONTROLLER_H

#include "ScenesFwd.h"
#include "Modules.h"
#include "ISceneFactory.h"

#include <vector>

namespace ChaiLatte
{
    namespace Scenes
    {
        class SceneController
        {
        public:
            SceneController(Modules::RenderingModule& renderingModule,
                            Modules::TextureModule& textureModule,
                            Modules::ShaderModule& shaderModule,
                            Modules::InputHandlingModule& inputHandlingModule);

            ~SceneController();

            void Update(float dt);

            void GoToNextScene();
            void GoToPreviousScene();
            
            template <typename TSceneFactory>
            void RegisterSceneFactory()
            {
                m_sceneFactories.push_back(new TSceneFactory(m_renderingModule,
                                                             m_textureModule,
                                                             m_shaderModule));
                
                if (m_currentSceneIndex < 0)
                {
                    m_currentSceneIndex = 0;
                    m_pCurrentScene = m_sceneFactories[m_currentSceneIndex]->CreateScene();
                }
            }

            template <typename TSceneFactory>
            void RegisterSceneFactoryWithInput()
            {
                m_sceneFactories.push_back(new TSceneFactory(m_renderingModule,
                m_textureModule,
                m_shaderModule,
                m_inputHandlingModule));

                if (m_currentSceneIndex < 0)
                {
                    m_currentSceneIndex = 0;
                    m_pCurrentScene = m_sceneFactories[m_currentSceneIndex]->CreateScene();
                }
            }

        private:
            std::vector<ISceneFactory*> m_sceneFactories;
            IScene* m_pCurrentScene;
            int m_currentSceneIndex;

            Modules::RenderingModule& m_renderingModule;
            Modules::TextureModule& m_textureModule;
            Modules::ShaderModule& m_shaderModule;
            Modules::InputHandlingModule& m_inputHandlingModule;
        };
    }
}

#endif //CHAILATTE_SCENECONTROLLER_H
