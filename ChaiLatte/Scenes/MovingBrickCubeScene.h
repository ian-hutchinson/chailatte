//
// Created by Ian Hutchinson on 27/07/15.
//

#ifndef CHAILATTE_MOVINGBRICKCUBESCENE_H
#define CHAILATTE_MOVINGBRICKCUBESCENE_H


#include <Textures/TextureRepository.h>
#include <Modules/TextureModule.h>
#include <Materials/DiffuseNormalMaterial.h>
#include "SceneBase.h"
#include "Mesh.h"
#include "Modules.h"
#include "Textures.h"
#include "Rendering.h"

namespace ChaiLatte
{
    namespace Scenes
    {
        class MovingBrickCubeScene : public SceneBase
        {
        public:
            MovingBrickCubeScene(const std::string& sceneName,
                                 Modules::RenderingModule& renderingModule,
                                 Modules::ShaderModule& shaderModule,
                                 Modules::TextureModule& textureModule);

            ~MovingBrickCubeScene();

            void Update(float dt);

        private:
            void LoadTexturesForScene(Modules::TextureModule& textureModule);
            void LoadModelsForScene(Modules::RenderingModule& renderingModule, Modules::ShaderModule& shaderModule);

        private:
            Rendering::Mesh* m_pCubeMesh;
            Rendering::Mesh* m_pFloorMesh;

            Rendering::Renderables::MeshRenderable* m_pCubeRenderable;
            Rendering::Renderables::MeshRenderable* m_pFloorRenderable;

            Materials::DiffuseNormalMaterial* m_pCubeMaterial;
            Materials::DiffuseNormalMaterial* m_pFloorMaterial;

            // Cube resource names
            std::string m_cubeModelName;
            std::string m_cubeDiffuseTextureName;
            std::string m_cubeNormalTextureName;

            // Ground resource names
            std::string m_floorModelName;
            std::string m_floorDiffuseTextureName;
            std::string m_floorNormalTextureName;

            Textures::TextureRepository& m_textureRepository;

            float m_sinWave;
        };
    }
}



#endif //CHAILATTE_MOVINGBRICKCUBESCENE_H
