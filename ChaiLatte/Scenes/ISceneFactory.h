//
// Created by Ian Hutchinson on 09/08/15.
//

#ifndef CHAILATTE_ISCENEFACTORY_H
#define CHAILATTE_ISCENEFACTORY_H

#include "ScenesFwd.h"

namespace ChaiLatte
{
    namespace Scenes
    {
        class ISceneFactory
        {
        public:
            virtual ~ISceneFactory()
            {

            }

            virtual IScene* CreateScene() = 0;
        };
    }
}

#endif //CHAILATTE_ISCENEFACTORY_H
