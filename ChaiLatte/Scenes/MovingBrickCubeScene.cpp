//
// Created by Ian Hutchinson on 27/07/15.
//

#include "MovingBrickCubeScene.h"
#include "TextureModule.h"
#include "TextureFactory.h"
#include "TextureRepository.h"
#include "Texture.h" // hmmm

#include "ShaderModule.h"
#include "ShaderRepository.h"

#include "RenderingModule.h"
#include "RenderQueue.h"
#include "MeshRenderable.h"


namespace ChaiLatte
{
    namespace Scenes
    {
        MovingBrickCubeScene::MovingBrickCubeScene(const std::string& sceneName,
                                                   Modules::RenderingModule& renderingModule,
                                                   Modules::ShaderModule& shaderModule,
                                                   Modules::TextureModule& textureModule)
        : SceneBase(sceneName)
        , m_cubeModelName("../../ChaiLatte/Resources/Models/cube.obj")
        , m_cubeDiffuseTextureName("bricks2.jpg")
        , m_cubeNormalTextureName("bricks2_normal.png")
        , m_floorModelName("../../ChaiLatte/Resources/Models/ground.dae")
        , m_floorDiffuseTextureName("mud_cracked.jpg")
        , m_floorNormalTextureName("default_normal.jpg")
        , m_textureRepository(textureModule.GetTextureRepository())
        , m_sinWave(0.f)
        {
            LoadTexturesForScene(textureModule);
            LoadModelsForScene(renderingModule, shaderModule);
        }

        MovingBrickCubeScene::~MovingBrickCubeScene()
        {

        }

        void MovingBrickCubeScene::Update(float dt)
        {
            m_sinWave += (1.f * dt);

            if (m_sinWave > 50000.f)
            {
                m_sinWave = 0.f;
            }

            auto transform = m_pCubeRenderable->GetTransform();
            transform.position.x = sin(m_sinWave) * 10.f;
            m_pCubeRenderable->SetTransform(transform);
        }

        void MovingBrickCubeScene::LoadTexturesForScene(Modules::TextureModule& textureModule)
        {
            auto& textureFactory = textureModule.GetTextureFactory();

            m_textureRepository.AddTexture(textureFactory.CreateTexture(m_cubeDiffuseTextureName));
            m_textureRepository.AddTexture(textureFactory.CreateTexture(m_cubeNormalTextureName));

            m_textureRepository.AddTexture(textureFactory.CreateTexture(m_floorDiffuseTextureName));
            m_textureRepository.AddTexture(textureFactory.CreateTexture(m_floorNormalTextureName));
        }

        void MovingBrickCubeScene::LoadModelsForScene(Modules::RenderingModule& renderingModule,
                                                      Modules::ShaderModule& shaderModule)
        {
            Shaders::ShaderRepository& shaderRepository = shaderModule.GetShaderRepository();
            Shaders::IShader& basicShader = shaderRepository.GetShader("basic_shader");

            m_pCubeMesh = new Rendering::Mesh();
            m_pFloorMesh = new Rendering::Mesh();

            auto& renderQueue = renderingModule.GetRenderQueue();

            if (m_pCubeMesh->LoadMeshFromFile(m_cubeModelName, false))
            {
                m_pCubeMaterial = new Materials::DiffuseNormalMaterial(basicShader,
                                                                       m_textureRepository.GetTexture(m_cubeDiffuseTextureName),
                                                                       m_textureRepository.GetTexture(m_cubeNormalTextureName));

                m_pCubeRenderable = new Rendering::Renderables::MeshRenderable(*m_pCubeMesh, *m_pCubeMaterial);

                auto cubeTransform = m_pCubeRenderable->GetTransform();
                cubeTransform.position.y = 5.f;
                m_pCubeRenderable->SetTransform(cubeTransform);

                renderQueue.Enqueue(*m_pCubeRenderable);
            }

            if (m_pFloorMesh->LoadMeshFromFile(m_floorModelName, true))
            {
                m_pFloorMaterial = new Materials::DiffuseNormalMaterial(basicShader,
                                                                        m_textureRepository.GetTexture(m_floorDiffuseTextureName),
                                                                        m_textureRepository.GetTexture(m_floorNormalTextureName));

                m_pFloorRenderable = new Rendering::Renderables::MeshRenderable(*m_pFloorMesh, *m_pFloorMaterial);
                renderQueue.Enqueue(*m_pFloorRenderable);
            }
        }
    }
}