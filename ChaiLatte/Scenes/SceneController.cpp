//
// Created by Ian Hutchinson on 09/08/15.
//

#include "SceneController.h"
#include "IScene.h"
#include "ISceneFactory.h"
#include <cassert>

namespace ChaiLatte
{
    namespace Scenes
    {
        SceneController::SceneController(Modules::RenderingModule& renderingModule,
                                         Modules::TextureModule& textureModule,
                                         Modules::ShaderModule& shaderModule,
                                         Modules::InputHandlingModule& inputHandlingModule)
        : m_pCurrentScene(nullptr)
        , m_currentSceneIndex(-1)
        , m_renderingModule(renderingModule)
        , m_textureModule(textureModule)
        , m_shaderModule(shaderModule)
        , m_inputHandlingModule(inputHandlingModule)
        {

        }

        SceneController::~SceneController()
        {
            if (m_pCurrentScene != nullptr)
            {
                delete m_pCurrentScene;
            }

            for (auto& sceneFactory : m_sceneFactories)
            {
                delete sceneFactory;
            }

            m_sceneFactories.clear();
        }

        void SceneController::Update(float dt)
        {
            if (m_pCurrentScene != nullptr)
            {
                m_pCurrentScene->Update(dt);
            }
        }

        void SceneController::GoToNextScene()
        {
            int lastIndex = m_currentSceneIndex;

            if (++m_currentSceneIndex > (m_sceneFactories.size() - 1))
            {
                m_currentSceneIndex = 0;
            }

            if (lastIndex != m_currentSceneIndex)
            {
                if (m_pCurrentScene != nullptr)
                {
                    delete m_pCurrentScene;
                    m_pCurrentScene = m_sceneFactories[m_currentSceneIndex]->CreateScene();
                }
            }
        }

        void SceneController::GoToPreviousScene()
        {
            int lastIndex = m_currentSceneIndex;

            if (--m_currentSceneIndex < 0)
            {
                m_currentSceneIndex = 0;
            }

            if (lastIndex != m_currentSceneIndex)
            {
                if (m_pCurrentScene != nullptr)
                {
                    delete m_pCurrentScene;
                    m_pCurrentScene = m_sceneFactories[m_currentSceneIndex]->CreateScene();
                }
            }
        }
    }
}