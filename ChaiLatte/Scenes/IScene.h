//
// Created by Ian Hutchinson on 26/07/15.
//

#ifndef CHAILATTE_ISCENE_H
#define CHAILATTE_ISCENE_H

#include <string>

namespace ChaiLatte
{
    namespace Scenes
    {
        class IScene
        {
        public:
            virtual ~IScene(){}

            virtual void Update(float dt) = 0;

            virtual const std::string& GetName() const = 0;
        };
    }
}

#endif //CHAILATTE_ISCENE_H
