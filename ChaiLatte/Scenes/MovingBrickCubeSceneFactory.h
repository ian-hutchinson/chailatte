//
// Created by Ian Hutchinson on 09/08/15.
//

#ifndef CHAILATTE_SCENEFACTORY_H
#define CHAILATTE_SCENEFACTORY_H

#include "ISceneFactory.h"
#include "Modules.h"
#include "MovingBrickCubeScene.h"
#include "Logger.h"

namespace ChaiLatte
{
    namespace Scenes
    {
        class MovingBrickCubeSceneFactory : public ISceneFactory
        {
        public:
            MovingBrickCubeSceneFactory(Modules::RenderingModule& renderingModule,
                                        Modules::TextureModule& textureModule,
                                        Modules::ShaderModule& shaderModule)
            : m_renderingModule(renderingModule)
            , m_textureModule(textureModule)
            , m_shaderModule(shaderModule)
            {

            }

            IScene* CreateScene()
            {
                CHAI_LOG("Creating MovingBrickCubeScene");
                return new MovingBrickCubeScene("MovingBrickCubeScene", m_renderingModule, m_shaderModule, m_textureModule);
            }

        private:
            Modules::RenderingModule& m_renderingModule;
            Modules::TextureModule& m_textureModule;
            Modules::ShaderModule& m_shaderModule;
        };
    }
}

#endif //CHAILATTE_SCENEFACTORY_H
