//
// Created by Ian Hutchinson on 27/07/15.
//

#include "SceneBase.h"

namespace ChaiLatte
{
    namespace Scenes
    {
        SceneBase::SceneBase(const std::string& sceneName)
        : m_name(sceneName)
        {

        }

        const std::string& SceneBase::GetName() const
        {
            return m_name;
        }
    }
}