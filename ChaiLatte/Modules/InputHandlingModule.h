//
// Created by Ian Hutchinson on 25/07/15.
//

#ifndef CHAILATTE_INPUTHANDLINGMODULE_H
#define CHAILATTE_INPUTHANDLINGMODULE_H

#include "Inputs.h"
#include <Include/GLFW/glfw3.h>
#include <Inputs/InputHandler.h>

namespace ChaiLatte
{
    namespace Modules
    {
        class InputHandlingModule
        {
        public:
            static InputHandlingModule* Create(GLFWwindow& window);

            Inputs::InputHandler& GetInputHandler();

            ~InputHandlingModule();
        private:
            InputHandlingModule(GLFWwindow& window);

            Inputs::InputHandler* m_pInputHandler;
        };
    }
}


#endif //CHAILATTE_INPUTHANDLINGMODULE_H
