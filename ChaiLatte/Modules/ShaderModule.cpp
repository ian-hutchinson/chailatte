//
//  ShaderModule.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 18/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "ShaderModule.h"
#include "ShaderRepository.h"
#include "ShaderFactory.h"
#include "Shader.h"

namespace ChaiLatte
{
    namespace Modules
    {
        ShaderModule::ShaderModule()
        {
            m_pShaderRepository = new Shaders::ShaderRepository();
            m_pShaderFactory = new Shaders::ShaderFactory();
            
            m_pShaderRepository->AddShader(m_pShaderFactory->CreateShader("basic_shader"));
            m_pShaderRepository->AddShader(m_pShaderFactory->CreateShader("colour_shader"));
        }
        
        ShaderModule::~ShaderModule()
        {
            delete m_pShaderFactory;
            delete m_pShaderRepository;
        }
        
        ShaderModule* ShaderModule::Create()
        {
            return new ShaderModule();
        }
        
        Shaders::ShaderRepository& ShaderModule::GetShaderRepository() const
        {
            return *m_pShaderRepository;
        }
    }
}