//
//  TextureModule.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 19/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "TextureModule.h"
#include "TextureRepository.h"
#include "TextureFactory.h"
#include "Texture.h"

namespace ChaiLatte
{
    namespace Modules
    {
        TextureModule::TextureModule()
        {
            m_pSoilImageLoader = new Images::SoilImageLoader();
            m_pTextureRepository = new Textures::TextureRepository();
            m_pTextureFactory = new Textures::TextureFactory(*m_pSoilImageLoader);
        }
        
        TextureModule::~TextureModule()
        {
            delete m_pTextureFactory;
            delete m_pTextureRepository;
            delete m_pSoilImageLoader;
        }
        
        TextureModule* TextureModule::Create()
        {
            return new TextureModule();
        }
        
        Textures::TextureRepository& TextureModule::GetTextureRepository() const
        {
            return *m_pTextureRepository;
        }

        Textures::TextureFactory& TextureModule::GetTextureFactory() const
        {
            return *m_pTextureFactory;
        }
    }
}