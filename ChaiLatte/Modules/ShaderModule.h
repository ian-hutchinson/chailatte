//
//  ShaderModule.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 18/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_ShaderModule_h
#define ChaiLatte_ShaderModule_h

#include "Shaders.h"

namespace ChaiLatte
{
    namespace Modules
    {
        class ShaderModule
        {
        public:
            ShaderModule();
            ~ShaderModule();
            
            static ShaderModule* Create();
            
            Shaders::ShaderRepository& GetShaderRepository() const;
        private:
            Shaders::ShaderFactory* m_pShaderFactory;
            Shaders::ShaderRepository* m_pShaderRepository;
        };
    }
}

#endif
