//
// Created by Ian Hutchinson on 25/07/15.
//

#include "InputHandlingModule.h"
#include "InputHandlerBase.h"
#include "InputHandler.h"

namespace ChaiLatte
{
    namespace Modules
    {
        InputHandlingModule::InputHandlingModule(GLFWwindow& window)
        {
            glfwSetKeyCallback(&window, Inputs::InputHandlerBase::KeyPressedCallbackDispatch);
            m_pInputHandler = new Inputs::InputHandler();
            m_pInputHandler->MakeActiveInputHandler();
        }

        InputHandlingModule::~InputHandlingModule()
        {
            delete m_pInputHandler;
        }

        InputHandlingModule* InputHandlingModule::Create(GLFWwindow& window)
        {
            return new InputHandlingModule(window);
        }

        Inputs::InputHandler& InputHandlingModule::GetInputHandler()
        {
            return *m_pInputHandler;
        }
    }
}