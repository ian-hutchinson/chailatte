//
//  RenderingModule.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 05/11/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "RenderingModule.h"
#include "RenderQueue.h"

namespace ChaiLatte
{
    namespace Modules
    {
        RenderingModule::RenderingModule()
        {
            m_pRenderQueue = new Rendering::RenderQueue();
        }
        
        RenderingModule::~RenderingModule()
        {
            delete m_pRenderQueue;
        }
        
        RenderingModule* RenderingModule::Create()
        {
            return new RenderingModule();
        }
        
        Rendering::RenderQueue& RenderingModule::GetRenderQueue() const
        {
            return *m_pRenderQueue;
        }
    }
}