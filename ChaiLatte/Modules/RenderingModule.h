//
//  RenderingModule.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 05/11/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__RenderingModule__
#define __ChaiLatte__RenderingModule__

#include "Rendering.h"

namespace ChaiLatte
{
    namespace Modules
    {
        class RenderingModule
        {
        public:
            RenderingModule();
            ~RenderingModule();
            
            static RenderingModule* Create();
            
            Rendering::RenderQueue& GetRenderQueue() const;
        private:
            Rendering::RenderQueue* m_pRenderQueue;
        };
    }
}
#endif /* defined(__ChaiLatte__RenderingModule__) */
