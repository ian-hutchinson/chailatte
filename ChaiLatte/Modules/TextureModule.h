//
//  TextureModule.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 19/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__TextureModule__
#define __ChaiLatte__TextureModule__

#include <Textures/TextureFactory.h>
#include "Textures.h"
#include "SoilImageLoader.h"

namespace ChaiLatte
{
    namespace Modules
    {
        class TextureModule
        {
        public:
            TextureModule();
            ~TextureModule();
            
            static TextureModule* Create();
        
            Textures::TextureRepository& GetTextureRepository() const;
            Textures::TextureFactory& GetTextureFactory() const;
        private:
            Textures::TextureRepository* m_pTextureRepository;
            Textures::TextureFactory* m_pTextureFactory;
            Images::SoilImageLoader* m_pSoilImageLoader;
        };
    }
}

#endif /* defined(__ChaiLatte__TextureModule__) */
