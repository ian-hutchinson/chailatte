//
//  Modules.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 05/11/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Modules_h
#define ChaiLatte_Modules_h

namespace ChaiLatte
{
    namespace Modules
    {
        class RenderingModule;
        class ShaderModule;
        class TextureModule;
        class InputHandlingModule;
    }
}

#endif
