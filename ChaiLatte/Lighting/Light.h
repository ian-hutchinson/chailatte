#pragma once

#include <glm/vec3.hpp>

namespace ChaiLatte
{
    namespace Lighting
    {
        class Light
        {
        public:
            Light();
            Light(const glm::vec3& position, const glm::vec3& intensities);
            
            void SetPosition(const glm::vec3& position);
            void SetIntensities(const glm::vec3& intensities);
            void SetAmbientCoefficient(const float ambientCoefficient);
            void SetAttenuation(const float attenuation);
            
            const glm::vec3& GetPosition() const;
            const glm::vec3& GetIntensities() const;
            const float GetAmbientCoefficient() const;
            const float GetAttenuation() const;
    
        private:
            glm::vec3 m_position;
            glm::vec3 m_intensities;
            float m_ambientCoefficient;
            float m_attenuation;
        };
    }
}