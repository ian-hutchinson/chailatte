//
//  Light.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 13/02/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "Light.h"

namespace ChaiLatte
{
    namespace Lighting
    {
        Light::Light() // TODO: Better default c'tor
        : m_ambientCoefficient(0.005f)
        {
            
        }
        
        Light::Light(const glm::vec3& position, const glm::vec3& intensities)
        : m_position(position)
        , m_intensities(intensities)
        , m_ambientCoefficient(0.005f)
        {
            
        }
        
        void Light::SetPosition(const glm::vec3 &position)
        {
            m_position = position;
        }
        
        void Light::SetIntensities(const glm::vec3 &intensities)
        {
            m_intensities = intensities;
        }
        
        void Light::SetAmbientCoefficient(const float ambientCoefficient)
        {
            m_ambientCoefficient = ambientCoefficient;
        }
        
        void Light::SetAttenuation(const float attenuation)
        {
            m_attenuation = attenuation;
        }
        
        const glm::vec3& Light::GetPosition() const
        {
            return m_position;
        }
        
        const glm::vec3& Light::GetIntensities() const
        {
            return m_intensities;
        }
        
        const float Light::GetAmbientCoefficient() const
        {
            return m_ambientCoefficient;
        }
        
        const float Light::GetAttenuation() const
        {
            return m_attenuation;
        }
    }
}