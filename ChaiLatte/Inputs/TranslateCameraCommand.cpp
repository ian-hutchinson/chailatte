//
//  TranslateCameraCommand.cpp
//  chailatte
//
//  Created by Ian Hutchinson on 02/12/2015.
//
//

#include "TranslateCameraCommand.h"
#include "Logger.h"
#include "Camera.h"

namespace ChaiLatte
{
    namespace Inputs
    {
        TranslateCameraCommand::TranslateCameraCommand(Cameras::Camera& camera, MovementDirection movementDirection)
        : m_camera(camera)
        , m_movementDirection(movementDirection)
        , m_movementSpeed(0.1f)
        {
            
        }
        
        void TranslateCameraCommand::Execute()
        {
            const auto& forward = m_camera.GetForward();
            const auto& position = m_camera.GetPosition();
            glm::vec3 newPosition;
            
            switch (m_movementDirection)
            {
                case Forward:
                {
                    newPosition = position + (forward * m_movementSpeed);
                    break;
                }
                case Backward:
                {
                    newPosition = position + (-forward * m_movementSpeed);
                    break;
                }
                case Left:
                {
                    const auto& up = m_camera.GetUp();
                    const auto& left = glm::normalize(glm::cross(up, forward));
                    newPosition = position + (left * m_movementSpeed);
                }
                case Right:
                {
                    const auto& up = m_camera.GetUp();
                    const auto& right = glm::normalize(glm::cross(forward, up));
                    newPosition = position + (right * m_movementSpeed);
                    break;
                }
                default:
                    break;
            }
            
            m_camera.SetPosition(newPosition);
        }
    }
}