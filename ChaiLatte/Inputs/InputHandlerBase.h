//
// Created by Ian Hutchinson on 25/07/15.
//

#ifndef CHAILATTE_INPUTHANDLERBASE_H
#define CHAILATTE_INPUTHANDLERBASE_H

#include <GLFW/glfw3.h>

namespace ChaiLatte
{
    namespace Inputs
    {
        class InputHandlerBase
        {
        public:
            virtual void KeyPressedCallback(GLFWwindow *window,
                                            int key,
                                            int scancode,
                                            int action,
                                            int mods) = 0;



            static void KeyPressedCallbackDispatch(GLFWwindow *window,
                                                   int key,
                                                   int scancode,
                                                   int action,
                                                   int mods)
            {
                if (pActiveInputHandler)
                {
                    pActiveInputHandler->KeyPressedCallback(window, key, scancode, action, mods);
                }
            }

            virtual void MakeActiveInputHandler()
            {
                pActiveInputHandler = this;
            }
            
            virtual ~InputHandlerBase(){}

        private:
            static InputHandlerBase* pActiveInputHandler;
        };
    }
}



#endif //CHAILATTE_INPUTHANDLERBASE_H
