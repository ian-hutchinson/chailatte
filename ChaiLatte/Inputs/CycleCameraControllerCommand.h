//
// Created by Ian Hutchinson on 25/07/15.
//

#ifndef CHAILATTE_CYCLECAMERACONTROLLERCOMMAND_H
#define CHAILATTE_CYCLECAMERACONTROLLERCOMMAND_H

#include <vector>

#include "ICommand.h"
#include "CamerasFwd.h"

namespace ChaiLatte
{
    namespace Inputs
    {
        class CycleCameraControllerCommand : public ICommand
        {
        public:
            CycleCameraControllerCommand(Cameras::Controllers::CameraControllerManager& cameraControllerManager,
                                         const std::vector<Cameras::Controllers::ICameraController*>& cameraControllers);

            void Execute();

        private:
            Cameras::Controllers::CameraControllerManager& m_cameraControllerManager;
            std::vector<Cameras::Controllers::ICameraController*> m_cameraControllers;
            unsigned int m_activeControllerIndex;
        };
    }
}

#endif //CHAILATTE_CYCLECAMERACONTROLLERCOMMAND_H
