//
//  TranslateCameraCommand.h
//  chailatte
//
//  Created by Ian Hutchinson on 02/12/2015.
//
//

#ifndef __chailatte__TranslateCameraCommand__
#define __chailatte__TranslateCameraCommand__

#include "ICommand.h"
#include "CamerasFwd.h"

namespace ChaiLatte
{
    namespace Inputs
    {
        enum MovementDirection
        {
            Forward,
            Backward,
            Left,
            Right
        };
        
        class TranslateCameraCommand : public ICommand
        {
        public:
            TranslateCameraCommand(Cameras::Camera& camera, MovementDirection movementDirection);
            void Execute();
            
        private:
            Cameras::Camera& m_camera;
            MovementDirection m_movementDirection;
            float m_movementSpeed;
        };
    }
}

#endif /* defined(__chailatte__TranslateCameraCommand__) */
