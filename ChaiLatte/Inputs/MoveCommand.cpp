//
//  MoveCommand.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 24/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "MoveCommand.h"

namespace ChaiLatte
{
    namespace Inputs
    {
        MoveCommand::MoveCommand(Objects::IMoveableObject& moveableObject)
        : m_moveableObject(moveableObject)
        , m_movementSpeed(0.01f)
        {
            
        }
        
        MoveCommand::~MoveCommand()
        {
            
        }
        
        void MoveCommand::Execute()
        {
            Rendering::Transform transform = m_moveableObject.GetTransform();
            transform.position.z -= m_movementSpeed;
            m_moveableObject.SetTransform(transform);
        }
    }
}