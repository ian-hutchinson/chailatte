//
//  SetCameraCommand.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 22/07/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__SetCameraCommand__
#define __ChaiLatte__SetCameraCommand__

#include "ICommand.h"
#include "CamerasFwd.h"

#include <glm/glm.hpp>

namespace ChaiLatte
{
    namespace Inputs
    {
        class SetCameraCommand : public ICommand
        {
        public:
            SetCameraCommand(Cameras::Camera& camera,
                             const glm::vec3& position);

            void Execute();
            
        private:
            Cameras::Camera& m_camera;
            glm::vec3 m_position;
        };
    }
}

#endif /* defined(__ChaiLatte__SetCameraCommand__) */
