//
//  InputHandler.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 28/04/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "InputHandler.h"
#include <cassert>

namespace ChaiLatte
{
    namespace Inputs
    {
        InputHandler::InputHandler()
        {
        }
        
        InputHandler::~InputHandler()
        {
            for (auto& keyCommandPair : m_keyCommandMap)
            {
                delete keyCommandPair.second;
            }
            
            m_keyCommandMap.clear();
        }
        
        void InputHandler::AddKeyCommand(int key, ICommand *pCommand)
        {
            TKeyCommandMap::iterator it = m_keyCommandMap.find(key);
            // Be harsh and assert for now. Add command overrides later.
            assert(it == m_keyCommandMap.end());
            
            m_keyCommandMap.insert(std::pair<int, ICommand*>(key, pCommand));
        }
        
        void InputHandler::RemoveCommandsForKey(int key)
        {
            TKeyCommandMap::iterator it = m_keyCommandMap.find(key);
            // Again, harsh for now
            assert(it != m_keyCommandMap.end());
            
            delete it->second;
            m_keyCommandMap.erase(it);
        }

        void InputHandler::KeyPressedCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
        {
            for (auto& keyCommandPair : m_keyCommandMap)
            {
                if (keyCommandPair.first == key)
                {
                    // Just working on single presses for now, can easily expand this
                    if (action == GLFW_PRESS || action == GLFW_REPEAT)
                    {
                        keyCommandPair.second->Execute();
                    }
                }
            }
        }
    }
}