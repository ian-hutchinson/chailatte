//
//  ICommand.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 24/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_ICommand_h
#define ChaiLatte_ICommand_h

namespace ChaiLatte
{
    namespace Inputs
    {
        class ICommand
        {
        public:
            virtual void Execute() = 0;
            virtual ~ICommand(){};
        };
    }
}

#endif
