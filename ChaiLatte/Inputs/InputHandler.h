//
//  InputHandler.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 28/04/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__InputHandler__
#define __ChaiLatte__InputHandler__

#include "ICommand.h"
#include <map>

#include "InputHandlerBase.h"

namespace ChaiLatte
{
    namespace Inputs
    {
        class InputHandler : public InputHandlerBase
        {
        public:
            InputHandler();
            ~InputHandler();
            
            void AddKeyCommand(int key, ICommand* pCommand);
            void RemoveCommandsForKey(int key);

            void KeyPressedCallback(GLFWwindow *window,
                                    int key,
                                    int scancode,
                                    int action,
                                    int mods);
            
        private:
            typedef std::map<int, ICommand*> TKeyCommandMap;
            
            TKeyCommandMap m_keyCommandMap;
        };
    }
}

#endif /* defined(__ChaiLatte__InputHandler__) */
