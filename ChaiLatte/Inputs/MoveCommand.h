//
//  MoveCommand.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 24/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_MoveCommand_h
#define ChaiLatte_MoveCommand_h

#include "ICommand.h"
#include "IMoveableObject.h"

namespace ChaiLatte
{
    namespace Inputs
    {
        class MoveCommand : public ICommand
        {
        public:
            MoveCommand(Objects::IMoveableObject& moveableObject);
            ~MoveCommand();
            void Execute();
            
            
        private:
            Objects::IMoveableObject& m_moveableObject;
            float m_movementSpeed;
        };
    }
}

#endif
