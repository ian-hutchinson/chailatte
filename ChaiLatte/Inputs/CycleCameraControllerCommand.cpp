//
// Created by Ian Hutchinson on 25/07/15.
//

#include "CycleCameraControllerCommand.h"
#include "CameraControllerManager.h"

namespace ChaiLatte
{
    namespace Inputs
    {
        CycleCameraControllerCommand::CycleCameraControllerCommand(
                Cameras::Controllers::CameraControllerManager& cameraControllerManager,
                const std::vector<Cameras::Controllers::ICameraController*>& cameraControllers)
        : m_cameraControllerManager(cameraControllerManager)
        , m_cameraControllers(cameraControllers)
        , m_activeControllerIndex(0)
        {

        }

        void CycleCameraControllerCommand::Execute()
        {
            m_activeControllerIndex++;
            printf("CycleCameraControllerCommand::Execute, index is %d\n", m_activeControllerIndex);

            if (m_activeControllerIndex > m_cameraControllers.size() - 1)
            {
                m_activeControllerIndex = 0;
            }

            auto pCameraController = m_cameraControllers.at(m_activeControllerIndex);
            m_cameraControllerManager.SetActiveController(pCameraController);
        }
    }
}