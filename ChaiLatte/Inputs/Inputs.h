//
// Created by Ian Hutchinson on 25/07/15.
//

#ifndef CHAILATTE_INPUTS_H
#define CHAILATTE_INPUTS_H

namespace ChaiLatte
{
    namespace Inputs
    {
        class InputHandlerBase;
        class InputHandler;

        class ICommand;
        class MoveCommand;
        class SetCameraCommand;
        class CycleCameraCommand;
    }
}

#endif //CHAILATTE_INPUTS_H
