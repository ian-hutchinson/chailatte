
//
//  SetCameraCommand.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 22/07/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "SetCameraCommand.h"
#include "Camera.h"

namespace ChaiLatte
{
    namespace Inputs
    {
        SetCameraCommand::SetCameraCommand(Cameras::Camera& camera,
                                           const glm::vec3& position)
        : m_camera(camera)
        , m_position(position)
        {
            
        }
        
        void SetCameraCommand::Execute()
        {
//            m_camera.SetPosition(m_position);
            
            auto position = m_camera.GetPosition();
            position.x += 0.5;
            m_camera.SetPosition(position);
        }
    }
}