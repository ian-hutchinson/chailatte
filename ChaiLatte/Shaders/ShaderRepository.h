//
//  ShaderRepository.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 28/04/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__ShaderRepository__
#define __ChaiLatte__ShaderRepository__

#include <map>
#include <string>
#include "Shaders.h"

namespace ChaiLatte
{
    namespace Shaders
    {
        typedef std::pair<std::string, IShader*> TShaderPair;
        
        class ShaderRepository
        {
        public:
            ShaderRepository();
            ~ShaderRepository();
            
            void AddShader(IShader* pShader);
            void RemoveShader(const std::string& name);
            IShader& GetShader(const std::string& name);
    
        private:
            typedef std::map<std::string, IShader*> TShaderMap;
            TShaderMap m_shaderMap;
        };
    }
}

#endif /* defined(__ChaiLatte__ShaderRepository__) */
