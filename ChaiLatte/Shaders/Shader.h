//
//  Shader.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 18/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Shader_h
#define ChaiLatte_Shader_h

#include "IShader.h"

#include <string>
#include <GL/glew.h>
#include <glm/detail/type_mat.hpp>

namespace ChaiLatte
{
    namespace Shaders
    {
        class Shader : public IShader
        {
        public:
            Shader(const std::string& name, GLuint program);
            ~Shader();
            
            void Bind();
            const std::string& GetName() const;
            GLuint GetProgram() const;

            void SetModel(const glm::mat4& model) const;
            void SetViewProjection(const glm::mat4& viewProjection) const;
            void SetCameraPosition(const glm::vec3& cameraPosition) const;
            
            const ShaderAttributes& GetShaderAttributes() const;

        private:
            const std::string m_name;
            GLuint m_program;
            ShaderAttributes* m_pShaderAttributes;
        };
    }
}

#endif
