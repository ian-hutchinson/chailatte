//
//  ShaderAttributes.h
//  chailatte
//
//  Created by Ian Hutchinson on 05/11/2015.
//
//

#ifndef __chailatte__ShaderAttributes__
#define __chailatte__ShaderAttributes__

#include <map>
#include "Types.h"
#include "Shaders.h"

namespace ChaiLatte
{
    namespace Shaders
    {
        class ShaderAttributes
        {
        public:
            ~ShaderAttributes();
            
            void CreateAddAttribute(const std::string& name, u32 index);
            
            bool HasAttributeWithName(const std::string& name);
            const ShaderAttribute& GetAttributeWithName(const std::string& name);
            
        private:
            std::map<std::string, ShaderAttribute*> m_shaderAttributes;
        };
    }
}

#endif /* defined(__chailatte__ShaderAttributes__) */
