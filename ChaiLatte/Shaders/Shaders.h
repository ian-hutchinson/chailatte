//
//  Shaders.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 18/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Shaders_h
#define ChaiLatte_Shaders_h

namespace ChaiLatte
{
    namespace Shaders
    {
        class IShader;
        class Shader;
        class ShaderLoader;
        class IShaderRepository;
        class ShaderRepository;
        class ShaderFactory;
        class ShaderAttribute;
        class ShaderAttributes;
    }
}

#endif
