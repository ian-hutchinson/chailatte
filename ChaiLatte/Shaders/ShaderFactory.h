//
//  ShaderFactory.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 18/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_ShaderFactory_h
#define ChaiLatte_ShaderFactory_h

#include "Shaders.h"

#include <string>
#include <vector>
#include <GL/glew.h>

namespace ChaiLatte
{
    namespace Shaders
    {
        class ShaderFactory
        {
        public:
            ShaderFactory();
            
            Shader* CreateShader(const std::string& shaderName);
        private:
            
            static const std::string VertexShaderExtension;
            static const std::string FragmentShaderExtension;
            std::string m_shaderSearchPath;
            
            std::string LoadShaderSource(const std::string& filename);
            GLuint CompileShader(const std::string& shaderSource, GLenum shaderType);
            GLuint CombineShaders(const std::vector<GLuint> shaderList);
            ShaderAttributes* ExtractShaderAttributes(GLuint program);
        };
    }
}

#endif
