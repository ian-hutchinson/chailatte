//
//  ShaderAttribute.cpp
//  chailatte
//
//  Created by Ian Hutchinson on 05/11/2015.
//
//

#include "ShaderAttribute.h"

namespace ChaiLatte
{
    namespace Shaders
    {

        ShaderAttribute::ShaderAttribute(const std::string& name,
                                         u32 index)
        : m_name(name)
        , m_index(index)
        {
            
        }
        
        const std::string& ShaderAttribute::GetName() const
        {
            return m_name;
        }
        
        u32 ShaderAttribute::GetIndex() const
        {
            return m_index;
        }
    }
}