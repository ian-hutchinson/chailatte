//
//  IShader.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 18/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_IShader_h
#define ChaiLatte_IShader_h

#include "Shaders.h"

#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>


namespace ChaiLatte
{
    namespace Shaders
    {
        class IShader
        {
        public:

            virtual ~IShader(){}

            virtual void Bind() = 0;
            virtual const std::string& GetName() const = 0;
            virtual const ShaderAttributes& GetShaderAttributes() const = 0;

            // TODO: This stuff needs to be a bit nicer...  <- Yes! generalise it.
            virtual void SetModel(const glm::mat4& model) const = 0;
            virtual void SetViewProjection(const glm::mat4& modelViewProjection) const = 0;
            virtual void SetCameraPosition(const glm::vec3& cameraPosition) const = 0;

            // This is temporary until I support uniforms correctly
            virtual GLuint GetProgram() const = 0;
        };
    }
}

#endif
