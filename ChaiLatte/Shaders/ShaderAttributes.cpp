//
//  ShaderAttributes.cpp
//  chailatte
//
//  Created by Ian Hutchinson on 05/11/2015.
//
//

#include "ShaderAttributes.h"
#include "ShaderAttribute.h"

#include <cassert>
#include <algorithm>

namespace ChaiLatte
{
    namespace Shaders
    {
        ShaderAttributes::~ShaderAttributes()
        {
            for (auto& mapping : m_shaderAttributes)
            {
                delete mapping.second;
            }
        }
        
        void ShaderAttributes::CreateAddAttribute(const std::string& name, u32 index)
        {
            assert(m_shaderAttributes.find(name) == m_shaderAttributes.end());
            
            ShaderAttribute* pAttribute = new ShaderAttribute(name, index);
            m_shaderAttributes[name] = pAttribute;
        }
        
        bool ShaderAttributes::HasAttributeWithName(const std::string& name)
        {
            return m_shaderAttributes.find(name) != m_shaderAttributes.end();
        }
        
        const ShaderAttribute& ShaderAttributes::GetAttributeWithName(const std::string& name)
        {
            return *m_shaderAttributes[name];
        }
    }
}