//
//  ShaderRepository.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 28/04/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "ShaderRepository.h"
#include "IShader.h"
#include "Logger.h"

using namespace ChaiLatte::Logging;

namespace ChaiLatte
{
    namespace Shaders
    {
        ShaderRepository::ShaderRepository()
        {
            
        }
        
        ShaderRepository::~ShaderRepository()
        {
            for (auto& shaderPair : m_shaderMap)
            {
                delete shaderPair.second;
            }
            
            m_shaderMap.clear();
        }
        
        void ShaderRepository::AddShader(IShader* pShader)
        {
            const std::string shaderName = pShader->GetName();
            if (m_shaderMap.find(shaderName) != m_shaderMap.end())
            {
                CHAI_LOG("Cannot add shader: " + shaderName + ", already exists\n");
                return;
            }
            
            m_shaderMap.insert(TShaderPair(pShader->GetName(), pShader));
        }
        
        void ShaderRepository::RemoveShader(const std::string& name)
        {
            TShaderMap::iterator it = m_shaderMap.find(name);
            assert(it != m_shaderMap.end());
            
            delete it->second;
            m_shaderMap.erase(it);
        }
        
        IShader& ShaderRepository::GetShader(const std::string& name)
        {
            TShaderMap::iterator it = m_shaderMap.find(name);
            assert(it != m_shaderMap.end());
            
            return *it->second;
        }
    }
}