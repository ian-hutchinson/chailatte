//
//  ShaderAttribute.h
//  chailatte
//
//  Created by Ian Hutchinson on 05/11/2015.
//
//

#ifndef __chailatte__ShaderAttribute__
#define __chailatte__ShaderAttribute__

#include <string>
#include "Types.h"

namespace ChaiLatte
{
    namespace Shaders
    {
        class ShaderAttribute
        {
        public:
            ShaderAttribute(const std::string& name,
                            u32 index);
            
            const std::string& GetName() const;
            u32 GetIndex() const;
        private:
            const std::string m_name;
            const u32 m_index;
        };
    }
}

#endif /* defined(__chailatte__ShaderAttribute__) */
