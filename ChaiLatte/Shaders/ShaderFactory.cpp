//
//  ShaderFactory.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 18/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "ShaderFactory.h"
#include "Shader.h"
#include "ShaderAttributes.h"
#include "Logger.h"

namespace ChaiLatte
{
    namespace Shaders
    {
        const std::string ShaderFactory::VertexShaderExtension   = ".vs";
        const std::string ShaderFactory::FragmentShaderExtension = ".fs";
        
        ShaderFactory::ShaderFactory()
        : m_shaderSearchPath("../../ChaiLatte/Resources/Shaders/")
        {
            
        }
        
        Shader* ShaderFactory::CreateShader(const std::string& shaderName)
        {
            GLuint shaderProgram;
            
            std::string vertexShaderFilename = m_shaderSearchPath + shaderName + VertexShaderExtension;
            std::string fragmentShaderFilename = m_shaderSearchPath + shaderName + FragmentShaderExtension;
            
            std::string vertexShaderSource   = LoadShaderSource(vertexShaderFilename);
            std::string fragmentShaderSource = LoadShaderSource(fragmentShaderFilename);
            
            std::vector<GLuint> shaderList;
            shaderList.push_back(CompileShader(vertexShaderSource, GL_VERTEX_SHADER));
            shaderList.push_back(CompileShader(fragmentShaderSource, GL_FRAGMENT_SHADER));
            
            shaderProgram = CombineShaders(shaderList);
            
            
            
            return new Shader(shaderName, shaderProgram);
        }
        
        std::string ShaderFactory::LoadShaderSource(const std::string& filename)
		{
            std::string sourceString;
            std::string scratch;
            std::ifstream sourceFile(filename, std::ifstream::in);
            
			CHAI_LOG("Trying to load source file at " + filename);
            assert(sourceFile.is_open());
            
            while (std::getline(sourceFile, scratch))
            {
                scratch.append("\n");
                sourceString.append(scratch);
            }
            
            return sourceString;
        }
        
        GLuint ShaderFactory::CompileShader(const std::string &shaderSource, GLenum shaderType)
        {
            GLuint shader = glCreateShader(shaderType);
            const GLchar* shaderSourceData = shaderSource.c_str();
            const GLint shaderSourceSize = static_cast<GLint>(shaderSource.length());
            glShaderSource(shader, 1, &shaderSourceData, &shaderSourceSize);
            
            glCompileShader(shader);
            
            GLint status;
            glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
            if (status == GL_FALSE)
            {
                GLint infoLogLength;
                glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
                
                GLchar *strInfoLog = new GLchar[infoLogLength + 1];
                glGetShaderInfoLog(shader, infoLogLength, NULL, strInfoLog);
                
                std::string errorString = "Compilation error in ";
                switch(shaderType)
                {
                    case GL_VERTEX_SHADER:   errorString += "vertex shader\n"  ; break;
                    case GL_GEOMETRY_SHADER: errorString += "geometry shader\n"; break;
                    case GL_FRAGMENT_SHADER: errorString += "fragment shader\n"; break;
                    default: break;
                }

                
                errorString.append(strInfoLog);
                errorString.append("\n\n\n");
                errorString.append(shaderSource);
                
                CHAI_LOG(errorString);
                printf("%s\n", errorString.c_str());
                
                delete[] strInfoLog;
            }
            
            return shader;
        }
        
        GLuint ShaderFactory::CombineShaders(const std::vector<GLuint> shaderList)
        {
            GLuint program = glCreateProgram();
        
            glBindAttribLocation(program, 0, "vertexPosition");
            glBindAttribLocation(program, 1, "texCoords");
            glBindAttribLocation(program, 2, "vertexNormal");
            glBindAttribLocation(program, 3, "tangent");
            
            for (const auto &shader : shaderList)
                glAttachShader(program, shader);
            
            glLinkProgram(program);
            
            // This is error checking gubbins. Can probably be factored out someplace.
            GLint status;
            glGetProgramiv(program, GL_LINK_STATUS, &status);
            
            if (status == GL_FALSE)
            {
                GLint infoLogLength;
                glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
                
                GLchar *strInfoLog = new GLchar[infoLogLength + 1];
                glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
                
                std::string errorString(strInfoLog);
                CHAI_LOG(errorString);
                
                delete[] strInfoLog;
            }
            
            for (const auto &shader : shaderList)
            {
                glDetachShader(program, shader);
                glDeleteShader(shader);
            }
            
            return program;
        }
    }
}