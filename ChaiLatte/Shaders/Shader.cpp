//
//  Shader.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 18/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "Shader.h"
#include "ShaderAttributes.h"

#include <glm/gtc/type_ptr.hpp>

namespace ChaiLatte
{
    namespace Shaders
    {
        namespace
        {
            ShaderAttributes* DetermineShaderAttributes(GLuint program)
            {
                int numberOfAttributes = -1;
                glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &numberOfAttributes);
                
                ShaderAttributes* pShaderAttributes = new ShaderAttributes();
                
                for (int i=0; i < numberOfAttributes; ++i)
                {
                    int lengthOfAttributeName = -1;
                    int unused = -1;
                    GLenum type = GL_ZERO;
                    char attributeName[100];
                    glGetActiveAttrib(program,
                                      GLuint(i),
                                      sizeof(attributeName)-1,
                                      &lengthOfAttributeName,
                                      &unused,
                                      &type,
                                      attributeName);
                    
                    attributeName[lengthOfAttributeName] = 0;
                    std::string attributeNameString(attributeName);
                    
                    GLuint attributeLocation = glGetAttribLocation(program, attributeName);
                    
                    pShaderAttributes->CreateAddAttribute(std::string(attributeName), attributeLocation);
                }
                
                return pShaderAttributes;
            }
    
        }
        
        Shader::Shader(const std::string& name, GLuint program)
        : m_name(name)
        , m_program(program)
        , m_pShaderAttributes(NULL)
        {
            m_pShaderAttributes = DetermineShaderAttributes(m_program);
        }
        
        Shader::~Shader()
        {
            glDeleteProgram(m_program);
            delete m_pShaderAttributes;
        }
        
        void Shader::Bind()
        {
            glUseProgram(m_program);
        }
        
        const std::string& Shader::GetName() const
        {
            return m_name;
        }
        
        GLuint Shader::GetProgram() const
        {
            return m_program;
        }
        
        const ShaderAttributes& Shader::GetShaderAttributes() const
        {
            assert(m_pShaderAttributes != NULL);
            return *m_pShaderAttributes;
        }
        
        // These methods are fail. Generalise.
        void Shader::SetModel(const glm::mat4& model) const
        {
            auto shaderModel = glGetUniformLocation(m_program, "model");
            glUniformMatrix4fv(shaderModel, 1, GL_FALSE, glm::value_ptr(model));
        }

        void Shader::SetViewProjection(const glm::mat4& viewProjection) const
        {
            auto shaderModel = glGetUniformLocation(m_program, "viewProjection");
            glUniformMatrix4fv(shaderModel, 1, GL_FALSE, glm::value_ptr(viewProjection));
        }

        void Shader::SetCameraPosition(const glm::vec3& cameraPosition) const
        {
            auto shaderCameraPosition = glGetUniformLocation(m_program, "cameraPosition");
            glUniform3fv(shaderCameraPosition, 1, glm::value_ptr(cameraPosition));
        }
    }
}