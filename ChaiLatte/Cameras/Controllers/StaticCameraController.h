//
// Created by Ian Hutchinson on 25/07/15.
//

#ifndef CHAILATTE_STATICCAMERACONTROLLER_H
#define CHAILATTE_STATICCAMERACONTROLLER_H

#include "ICameraController.h"

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {
            class StaticCameraController : public ICameraController
            {
            public:
                StaticCameraController(Camera& camera);
                void Update();
                const Camera& GetCamera() const;

            private:
                Camera& m_camera;
            };
        }
    }
}


#endif //CHAILATTE_STATICCAMERACONTROLLER_H
