//
//  FreeFlyCameraController.h
//  chailatte
//
//  Created by Ian Hutchinson on 02/12/2015.
//
//

#ifndef __chailatte__FreeFlyCameraController__
#define __chailatte__FreeFlyCameraController__

#include "ICameraController.h"
#include "CamerasFwd.h"
#include "Inputs.h"
#include "ICommand.h"
#include <map>

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {
            class FreeFlyCameraController : public ICameraController
            {
            public:
                FreeFlyCameraController(Camera& camera, Inputs::InputHandler& inputHandler);
                ~FreeFlyCameraController();
                void Update();
                const Camera& GetCamera() const;
                
            private:
                Camera& m_camera;
                Inputs::InputHandler& m_inputHandler;
                
                std::map<int, Inputs::ICommand*> m_commands;
            };
        }
    }
}

#endif /* defined(__chailatte__FreeFlyCameraController__) */
