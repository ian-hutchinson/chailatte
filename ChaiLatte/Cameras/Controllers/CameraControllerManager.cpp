//
// Created by Ian Hutchinson on 25/07/15.
//

#include <assert.h>
#include "CameraControllerManager.h"

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {
            CameraControllerManager::CameraControllerManager()
            : m_pActiveController(nullptr)
            {

            }

            void CameraControllerManager::RegisterController(ICameraController* pCameraController)
            {
                auto it = std::find(m_registeredControllers.begin(), m_registeredControllers.end(), pCameraController);
                assert(it == m_registeredControllers.end());

                m_registeredControllers.push_back(pCameraController);

                if (!m_pActiveController)
                {
                    SetActiveController(pCameraController);
                }
            }

            void CameraControllerManager::UnregisterController(ICameraController* pCameraController)
            {
                // TODO: Implement this
            }

            ICameraController& CameraControllerManager::GetActiveController()
            {
                assert(m_pActiveController != nullptr);
                return *m_pActiveController;
            }

            void CameraControllerManager::SetActiveController(ICameraController* pCameraController)
            {
                auto it = std::find(m_registeredControllers.begin(), m_registeredControllers.end(), pCameraController);
                assert(it != m_registeredControllers.end());

                m_pActiveController = pCameraController;
            }
        }
    }
}