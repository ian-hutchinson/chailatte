//
// Created by Ian Hutchinson on 25/07/15.
//

#ifndef CHAILATTE_CAMERACONTROLLERMANAGER_H
#define CHAILATTE_CAMERACONTROLLERMANAGER_H

#include <vector>
#include "CamerasFwd.h"

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {
            class CameraControllerManager
            {
            public:
                CameraControllerManager();

                void RegisterController(ICameraController* pCameraController);
                void UnregisterController(ICameraController* pCameraController);

                ICameraController& GetActiveController();
                void SetActiveController(ICameraController* pCameraController);
            private:
                std::vector<ICameraController*> m_registeredControllers;
                ICameraController* m_pActiveController;
            };
        }
    }
}

#endif //CHAILATTE_CAMERACONTROLLERMANAGER_H
