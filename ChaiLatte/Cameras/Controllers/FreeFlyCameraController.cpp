//
//  FreeFlyCameraController.cpp
//  chailatte
//
//  Created by Ian Hutchinson on 02/12/2015.
//
//

#include "FreeFlyCameraController.h"
#include "TranslateCameraCommand.h"
#include "InputHandler.h"

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {
            FreeFlyCameraController::FreeFlyCameraController(Camera& camera, Inputs::InputHandler& inputHandler)
            : m_camera(camera)
            , m_inputHandler(inputHandler)
            {
                m_commands.insert(std::make_pair(GLFW_KEY_W, new Inputs::TranslateCameraCommand(camera, Inputs::Forward)));
                m_commands.insert(std::make_pair(GLFW_KEY_S, new Inputs::TranslateCameraCommand(camera, Inputs::Backward)));
                m_commands.insert(std::make_pair(GLFW_KEY_A, new Inputs::TranslateCameraCommand(camera, Inputs::Left)));
                m_commands.insert(std::make_pair(GLFW_KEY_D, new Inputs::TranslateCameraCommand(camera, Inputs::Right)));
                
                for (const auto& command : m_commands)
                {
                    m_inputHandler.AddKeyCommand(command.first, command.second);
                }
            }
            
            FreeFlyCameraController::~FreeFlyCameraController()
            {
                for (const auto& command : m_commands)
                {
                    m_inputHandler.RemoveCommandsForKey(command.first);
                    //delete command.second; Leak for now. Crash on teardown
                }
                
                m_commands.clear();
            }
            
            void FreeFlyCameraController::Update()
            {
                
            }
            
            const Camera& FreeFlyCameraController::GetCamera() const
            {
                return m_camera;
            }
        }
    }
}