//
// Created by Ian Hutchinson on 25/07/15.
//

#include "StaticCameraController.h"
#include "Camera.h"

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {

            StaticCameraController::StaticCameraController(Camera& camera)
            : m_camera(camera)
            {

            }

            void StaticCameraController::Update()
            {
            }

            const Camera& StaticCameraController::GetCamera() const
            {
                return m_camera;
            }
        }
    }
}
