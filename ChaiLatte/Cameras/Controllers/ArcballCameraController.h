//
//  ArcballCameraController.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 22/07/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__ArcballCameraController__
#define __ChaiLatte__ArcballCameraController__

#include "ICameraController.h"
#include "CamerasFwd.h"

#include <glm/glm.hpp>

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {
            class ArcballCameraController : public ICameraController
            {
            public:
                ArcballCameraController(Camera& camera);
                
                void Update();

                const Camera& GetCamera() const;
                
            private:
                Camera& m_camera;
                glm::vec3 m_targetPosition;
            };
        }
    }
}

#endif /* defined(__ChaiLatte__ArcballCameraController__) */
