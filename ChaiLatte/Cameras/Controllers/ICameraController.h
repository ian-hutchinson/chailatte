//
//  ICameraController.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 22/07/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_ICameraController_h
#define ChaiLatte_ICameraController_h

#include "CamerasFwd.h"

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {
            class ICameraController
            {
            public:
                virtual ~ICameraController(){}
                virtual void Update() = 0;
                virtual const Camera& GetCamera() const = 0;
            };
        }
    }
}

#endif
