//
//  ArcballCameraController.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 22/07/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "ArcballCameraController.h"
#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>

namespace ChaiLatte
{
    namespace Cameras
    {
        namespace Controllers
        {
            ArcballCameraController::ArcballCameraController(Camera& camera)
            : m_camera(camera)
            , m_targetPosition(m_camera.GetPosition())
            {
                
            }
            
            void ArcballCameraController::Update()
            {
                const auto& cameraPosition = m_camera.GetPosition();
                const auto& arcballCentre = m_camera.GetLookAt();
                auto centreToCamera = glm::vec4(cameraPosition - arcballCentre, 0);

                const auto& worldUp = glm::vec3(0, 1, 0);
                const auto rotationAmount = 0.3f;
                glm::mat4 rotationMatrix;
                rotationMatrix = glm::rotate(rotationMatrix, glm::radians(rotationAmount), worldUp);

                auto newCameraPosition = centreToCamera * rotationMatrix;

                auto vec3CameraPosition = glm::vec3(newCameraPosition.x,
                                                    newCameraPosition.y,
                                                    newCameraPosition.z);
                
                vec3CameraPosition += arcballCentre;

                m_camera.SetPosition(vec3CameraPosition);
            }

            const Camera& ArcballCameraController::GetCamera() const
            {
                return m_camera;
            }
        }
    }
}