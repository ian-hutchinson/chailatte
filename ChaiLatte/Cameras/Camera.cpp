//
//  Cameras.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 23/09/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace ChaiLatte
{
    namespace Cameras
    {
        Camera::Camera(const glm::vec3& position, float fov, float aspectRatio, float near, float far)
        : m_position(position)
        , m_forward(glm::vec3(0, 0, -1))
        , m_up(glm::vec3(0, 1, 0))
        , m_lookAt(glm::vec3(0, 0, 0))
        {
            m_projectionMatrix = glm::perspective(glm::radians(fov), aspectRatio, near, far);
        }

        void Camera::SetLookAt(const glm::vec3& lookAtTarget)
        {
            m_lookAt = lookAtTarget;
            RecalculateAxis();
        }
        
        void Camera::SetPosition(const glm::vec3& position)
        {
            m_position = position;
            RecalculateAxis();
        }
        
        void Camera::SetUp(const glm::vec3& up)
        {
            m_up = up;
        }
        
        glm::vec3 Camera::GetLookAt() const
        {
            return m_lookAt;
        }
        
        glm::vec3 Camera::GetPosition() const
        {
            return m_position;
        }
        
        glm::vec3 Camera::GetUp() const
        {
            return m_up;
        }
        
        glm::vec3 Camera::GetForward() const
        {
            return m_forward;
        }
        
        glm::mat4 Camera::GetViewProjectionMatrix() const
        {
            return m_projectionMatrix * glm::lookAt(m_position, m_lookAt, m_up);
        }
        
        void Camera::RecalculateAxis()
        {
            m_forward = glm::normalize(m_lookAt - m_position);
        }
    }
}