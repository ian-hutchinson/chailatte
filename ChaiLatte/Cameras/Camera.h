//
//  RenderCamera.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 23/09/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__RenderCamera__
#define __ChaiLatte__RenderCamera__

#include <glm/glm.hpp>

namespace ChaiLatte
{
    namespace Cameras
    {
        class Camera
        {
        public:
            Camera(const glm::vec3& position, float fov, float aspect, float near, float far);
           
            void SetLookAt(const glm::vec3& lookAtTarget);
            void SetPosition(const glm::vec3& position);
            void SetUp(const glm::vec3& up);
        
            glm::vec3 GetLookAt() const;
            glm::vec3 GetPosition() const;
            glm::vec3 GetUp() const;
            glm::vec3 GetForward() const;
            glm::mat4 GetViewProjectionMatrix() const;

        private:
            void RecalculateAxis();

        private:
            glm::mat4 m_projectionMatrix;
            
            glm::vec3 m_position;
            glm::vec3 m_forward;
            glm::vec3 m_up;
            
            glm::vec3 m_lookAt;
        };
    }
}

#endif /* defined(__ChaiLatte__RenderCamera__) */
