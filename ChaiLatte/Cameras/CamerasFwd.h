//
//  Cameras.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 23/09/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Camera_h
#define ChaiLatte_Camera_h

namespace ChaiLatte
{
    namespace Cameras
    {
        class Camera;
        
        namespace Controllers
        {
            class CameraControllerManager;
            class ICameraController;
            class ArcballCameraController;
            class StaticCameraController;
        }
    }
}

#endif
