//
//  App.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 24/02/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__App__
#define __ChaiLatte__App__

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "AppConfig.h"
#include "CamerasFwd.h"
#include "ScenesFwd.h"
#include "Modules.h"
#include "Rendering.h"
#include "Light.h"
#include "InputHandler.h" // Lashup
#include "MoveCommand.h"
#include "SetCameraCommand.h"
#include "CycleCameraControllerCommand.h"

#include <glm/glm.hpp>

#include "ScenesFwd.h"

namespace ChaiLatte
{
    namespace Application
    {
        class App
        {
        public:
            App(AppConfig config = AppConfig::CreateDefaultConfig());
            ~App();

            void Run();

        private:
            void Initialise();
            void Update(float dt);
            void Draw();
            void InitialiseLight(); // TEMP!!!
            void InitialiseGLFWWindow();
            void InitialiseGLEW() const;

        private:
            GLFWwindow *m_pWindow;

            Cameras::Camera* m_pCamera;
            Lighting::Light* m_pLight;
            
            Modules::RenderingModule* m_pRenderingModule;
            Modules::ShaderModule* m_pShaderModule;
            Modules::TextureModule* m_pTextureModule;
            Modules::InputHandlingModule* m_pInputHandlingModule;

            Cameras::Controllers::CameraControllerManager* m_pCameraControllerManager;
            Cameras::Controllers::ICameraController* m_pStaticCameraController;
            Cameras::Controllers::ICameraController* m_pTempFreeflyCameraController;

            // TODO: Interface for this once happy with public methods
            Scenes::SceneController* m_pSceneController;

            bool m_running;
            
            AppConfig m_config;
        };
    }
}

#endif /* defined(__ChaiLatte__App__) */
