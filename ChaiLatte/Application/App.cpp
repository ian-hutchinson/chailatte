//
//  App.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 24/02/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "App.h"
#include "Logger.h"
#include "Camera.h"
#include "RenderingModule.h"
#include "ShaderModule.h"
#include "ShaderRepository.h"
#include "TextureModule.h"
#include "InputHandlingModule.h"
#include "SceneController.h"

#include "RenderQueue.h"
#include "IShader.h" // Temp until that shit in the update is sorted

#include <glm/gtc/type_ptr.hpp>

#include "StaticCameraController.h"
#include "FreeFlyCameraController.h"
#include "CameraControllerManager.h"

#include "MovingBrickCubeSceneFactory.h"

#include "VertexFlags.h"
#include <sstream>

namespace ChaiLatte
{
    using namespace Logging;
    
    namespace Application
    {
        App::App(AppConfig config)
        : m_running(true)
        , m_config(config)
        {
            InitialiseGLFWWindow();
            InitialiseGLEW();
            Initialise();
        }
        
        App::~App()
        {
            delete m_pSceneController;

            delete m_pCameraControllerManager;
            delete m_pTempFreeflyCameraController;
            delete m_pStaticCameraController;
            delete m_pCamera;

            delete m_pInputHandlingModule;
            delete m_pTextureModule;
            delete m_pShaderModule;
            delete m_pRenderingModule;
            
            glfwTerminate();
        }
        
        void App::Initialise()
        {
            m_pRenderingModule = Modules::RenderingModule::Create();
            m_pShaderModule = Modules::ShaderModule::Create();
            m_pTextureModule = Modules::TextureModule::Create();
            m_pInputHandlingModule = Modules::InputHandlingModule::Create(*m_pWindow);
            
            const float aspectRatio = m_config.screenWidth / m_config.screenHeight;
            m_pCamera = new Cameras::Camera(glm::vec3(0, 10.f, 20), 45.f, aspectRatio, 0.001f, 1000.f);
            m_pCamera->SetLookAt(glm::vec3(0, 0, 0));

            InitialiseLight();

            m_pStaticCameraController = new Cameras::Controllers::StaticCameraController(*m_pCamera);
            m_pTempFreeflyCameraController = new Cameras::Controllers::FreeFlyCameraController(*m_pCamera,
                                                                                               m_pInputHandlingModule->GetInputHandler());
            m_pCameraControllerManager = new Cameras::Controllers::CameraControllerManager();
            m_pCameraControllerManager->RegisterController(m_pTempFreeflyCameraController);

            m_pSceneController = new Scenes::SceneController(*m_pRenderingModule,
                                                             *m_pTextureModule,
                                                             *m_pShaderModule,
                                                             *m_pInputHandlingModule);

            m_pSceneController->RegisterSceneFactory<Scenes::MovingBrickCubeSceneFactory>();
            
            std::stringstream ss;
            
            ss << Rendering::VertexFlags::Position;
            CHAI_LOG("Position value: " + ss.str());
            ss.str(std::string());
            
            ss << Rendering::VertexFlags::Normal;
            CHAI_LOG("Normal value: " + ss.str());
            ss.str(std::string());
            
            ss << Rendering::VertexFlags::UV;
            CHAI_LOG("UV value: " + ss.str());
            ss.str(std::string());
        }

        void App::Run()
        {
            double lastTime = glfwGetTime();
            while (m_running)
            {
                double currentTime = glfwGetTime();
                double elapsed = currentTime - lastTime;
				lastTime = currentTime;

                Update(elapsed);
                InitialiseLight();
                Draw();
            }
        }
        
        void App::InitialiseLight()
        {
            m_pLight = new Lighting::Light();
            m_pLight->SetPosition(glm::vec3(0.f,15.f, -5.f));
            m_pLight->SetIntensities(glm::vec3(1, 1, 1));
            m_pLight->SetAmbientCoefficient(0.33f);
            m_pLight->SetAttenuation(0.001f);

            Shaders::ShaderRepository& shaderRepository = m_pShaderModule->GetShaderRepository();
            Shaders::IShader& shader = shaderRepository.GetShader("basic_shader");
            
            shader.Bind();
            auto program = shader.GetProgram();
            
            auto shaderLightPosition = glGetUniformLocation(program, "light.position");
            glUniform3fv(shaderLightPosition, 1, glm::value_ptr(m_pLight->GetPosition()));
            
            auto shaderLightIntensities = glGetUniformLocation(program, "light.intensities");
            glUniform3fv(shaderLightIntensities, 1, glm::value_ptr(m_pLight->GetIntensities()));
            
            auto shaderAmbientCoefficient = glGetUniformLocation(program, "light.ambientCoefficient");
            glUniform1f(shaderAmbientCoefficient, m_pLight->GetAmbientCoefficient());
            
            auto shaderAttenuation = glGetUniformLocation(program, "light.attenuation");
            glUniform1f(shaderAttenuation, m_pLight->GetAttenuation());
        }
        
        void App::Update(float dt)
        {
            auto& activeCameraController = m_pCameraControllerManager->GetActiveController();
            activeCameraController.Update();

            m_pSceneController->Update(dt);
            
            glfwPollEvents();
            m_running = !glfwWindowShouldClose(m_pWindow);
        }
        
        void App::Draw()
        {
            Rendering::RenderQueue& renderQueue = m_pRenderingModule->GetRenderQueue();

            const auto& activeCameraController = m_pCameraControllerManager->GetActiveController();
            const Cameras::Camera& activeCamera = activeCameraController.GetCamera();
            
            renderQueue.Render(activeCamera);
            glfwSwapBuffers(m_pWindow);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
        
        void App::InitialiseGLFWWindow()
        {
            if (!glfwInit())
            {
                CHAI_LOG("Couldn't initialise GLFW");
            }
            

            CHAI_LOG("Trying to get best GL context");
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
            glfwWindowHint(GLFW_SAMPLES, 4);

            glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
            m_pWindow = glfwCreateWindow(m_config.screenWidth, m_config.screenHeight, "ChaiLatte", NULL, NULL);
            
            assert(m_pWindow != NULL);
            
            glfwMakeContextCurrent(m_pWindow);

            std::string version = "OpenGL Version: ";
            version.append(reinterpret_cast<const char*>(glGetString(GL_VERSION)));
            CHAI_LOG(version);
        }
        
        
        void App::InitialiseGLEW() const
        {
            glewExperimental = GL_TRUE;
            const GLenum errorCode = glewInit();
            
            if (errorCode != GLEW_OK)
            {
                CHAI_LOG("glewInit didn't return GLEW_OK. Fine if experimental");
            }
            
            glDepthFunc(GL_LESS);
            glEnable(GL_BLEND);
            glEnable(GL_DEPTH_TEST);
            glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glClearColor (0.5, 0.5, 0.7, 1.0f);
        }
    }
}