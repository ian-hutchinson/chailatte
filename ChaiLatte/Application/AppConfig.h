//
//  AppConfig.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 04/09/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__AppConfig__
#define __ChaiLatte__AppConfig__

namespace ChaiLatte
{
    namespace Application
    {
        class AppConfig
        {
        public:
            float screenHeight;
            float screenWidth;

            static AppConfig CreateDefaultConfig();
        };
    }
}

#endif /* defined(__ChaiLatte__AppConfig__) */
