//
//  AppConfig.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 04/09/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "AppConfig.h"

namespace ChaiLatte
{
    namespace Application
    {
        AppConfig AppConfig::CreateDefaultConfig()
        {
            AppConfig config;
            
            config.screenWidth = 1280.f;
            config.screenHeight = 720.f;
            
            return config;
        }
    }
}