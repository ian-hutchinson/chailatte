//
//  DiffuseNormalMaterial.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 26/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__DiffuseMaterial__
#define __ChaiLatte__DiffuseMaterial__

#include "IMaterial.h"
#include "Textures.h"

namespace ChaiLatte
{
    namespace Materials
    {
        class DiffuseNormalMaterial : public IMaterial
        {
        public:
            DiffuseNormalMaterial(Shaders::IShader& shader,
                                  Textures::ITexture& diffuseTexture,
                                  Textures::ITexture& normalTexture);

            ~DiffuseNormalMaterial();

            void Prepare(Rendering::Renderables::RenderableBase& renderable, const Cameras::Camera& camera);
            void CleanUp();

        private:
            void BindTextures();
            void UnbindTextures();

            Shaders::IShader& m_shader;
            Textures::ITexture& m_diffuseTexture;
            Textures::ITexture& m_normalTexture;
        };
    }
}

#endif /* defined(__ChaiLatte__DiffuseMaterial__) */
