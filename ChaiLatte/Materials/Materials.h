//
//  Materials.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 26/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Materials_h
#define ChaiLatte_Materials_h

namespace ChaiLatte
{
    namespace Materials
    {
        class IMaterial;
        class DiffuseNormalMaterial;
    }
}

#endif
