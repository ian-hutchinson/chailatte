//
//  IMaterial.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 26/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_IMaterial_h
#define ChaiLatte_IMaterial_h

#include "Shaders.h"
#include "Rendering.h"
#include "CamerasFwd.h"

namespace ChaiLatte
{
    namespace Materials
    {
        class IMaterial
        {
        public:
            virtual ~IMaterial(){}
            virtual void Prepare(Rendering::Renderables::RenderableBase& renderable, const Cameras::Camera& camera) = 0;
            virtual void CleanUp() = 0;
        };
    }
}

#endif
