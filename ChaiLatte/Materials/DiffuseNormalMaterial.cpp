//
//  DiffuseNormalMaterial.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 26/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include <glm/gtc/type_ptr.hpp>
#include "DiffuseNormalMaterial.h"
#include "IShader.h"
#include "ITexture.h"
#include "RenderableBase.h"
#include "Camera.h"

namespace ChaiLatte
{
    namespace Materials
    {
        DiffuseNormalMaterial::DiffuseNormalMaterial(Shaders::IShader& shader, // Maybe concretion here
                                                     Textures::ITexture& diffuseTexture,
                                                     Textures::ITexture& normalTexture)
        : m_shader(shader)
        , m_diffuseTexture(diffuseTexture)
        , m_normalTexture(normalTexture)
        {
            
        }

        DiffuseNormalMaterial::~DiffuseNormalMaterial()
        {

        }

        void DiffuseNormalMaterial::BindTextures()
        {
            m_shader.Bind();
            GLuint program = m_shader.GetProgram();

            m_diffuseTexture.Bind(0);
            auto textureLocation = glGetUniformLocation(program, "materialTexture");
            glUniform1i(textureLocation, 0);

            m_normalTexture.Bind(1);
            auto normalTextureLocation = glGetUniformLocation(program, "normalTexture");
            glUniform1i(normalTextureLocation, 1);
        }

        void DiffuseNormalMaterial::UnbindTextures()
        {
            m_diffuseTexture.Unbind();
            m_normalTexture.Unbind();
        }

        void DiffuseNormalMaterial::Prepare(Rendering::Renderables::RenderableBase& renderable, const Cameras::Camera& camera)
        {
            BindTextures();
            m_shader.SetViewProjection(camera.GetViewProjectionMatrix());
            m_shader.SetModel(renderable.GetModel());
            m_shader.SetCameraPosition(camera.GetPosition());

            auto program = m_shader.GetProgram();

            // Hard-code these for now
            const float shininess = 100.f;
            auto shaderMaterialShininess = glGetUniformLocation(program, "materialShininess");
            glUniform1f(shaderMaterialShininess, shininess);

            glm::vec3 specularColour = glm::vec3(1.f, 1.f, 1.f);
            auto shaderSpecularColour = glGetUniformLocation(program, "materialSpecularColour");
            glUniform3fv(shaderSpecularColour, 1, glm::value_ptr(specularColour));
        }

        void DiffuseNormalMaterial::CleanUp()
        {
            UnbindTextures();
        }
    }
}