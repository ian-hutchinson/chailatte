//
//  DebugLineRenderable.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 23/06/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__DebugLineRenderable__
#define __ChaiLatte__DebugLineRenderable__

#include "IRenderable.h"
#include "Shaders.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include <vector>

namespace ChaiLatte
{
    namespace Debug
    {
        class DebugLineRenderable : public Rendering::Renderables::IRenderable
        {
        public:
            DebugLineRenderable(Shaders::IShader& shader, const std::vector<glm::vec3>& vertices, const glm::vec4& colour);
            
            void Render(const Cameras::Camera& camera);
            
        private:
            enum Buffers
            {
                VertexBuffer,
                ColourBuffer,
                BufferSize
            };
            
            Shaders::IShader& m_shader;
            GLuint m_vao;
            GLuint m_vbo[BufferSize];
            unsigned long m_elementCount;
            
            std::vector<glm::vec3> m_vertices;
        };
    }
}

#endif /* defined(__ChaiLatte__DebugLineRenderable__) */
