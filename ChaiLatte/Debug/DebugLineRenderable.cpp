//
//  DebugLineRenderable.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 23/06/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "DebugLineRenderable.h"
#include "Camera.h"
#include "IShader.h"
#include "Transform.h"

#include <glm/gtc/type_ptr.hpp>

namespace ChaiLatte
{
    namespace Debug
    {
        DebugLineRenderable::DebugLineRenderable(Shaders::IShader& shader, const std::vector<glm::vec3>& vertices, const glm::vec4& colour)
        : m_shader(shader)
        , m_elementCount(vertices.size())
        , m_vertices(vertices)
        {
            for (int i = 0; i < BufferSize; ++i)
            {
                m_vbo[i] = 0;
            }
            
            glGenVertexArrays(1, &m_vao);
            glBindVertexArray(m_vao);
            
            const unsigned long numVerts = vertices.size();
            float* verts = new float[numVerts * 3];
            
            for (unsigned long i = 0; i < numVerts; ++i)
            {
                verts[i * 3] = vertices.at(i).x;
                verts[i * 3 + 1] = vertices.at(i).y;
                verts[i * 3 + 2] = vertices.at(i).z;
            }
            
            glGenBuffers(1, &m_vbo[VertexBuffer]);
            glBindBuffer(GL_ARRAY_BUFFER, m_vbo[VertexBuffer]);
            glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), verts, GL_STATIC_DRAW);
            
            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

            float* colours = new float[numVerts * 4];
            
            for (unsigned long i = 0; i < numVerts; ++i)
            {
                colours[i * 4] = colour.r;
                colours[i * 4 + 1] = colour.g;
                colours[i * 4 + 2] = colour.b;
                colours[i * 4 + 3] = colour.a;
            }
  
            glGenBuffers(1, &m_vbo[ColourBuffer]);
            glBindBuffer(GL_ARRAY_BUFFER, m_vbo[ColourBuffer]);
            glBufferData(GL_ARRAY_BUFFER, 4 * numVerts * sizeof(GLfloat), colours, GL_DYNAMIC_DRAW);
            
            glEnableVertexAttribArray(1);
            glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
            
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
            
            delete verts;
            delete colours;
        }
        
        void DebugLineRenderable::Render(const Cameras::Camera& camera)
        {
            m_shader.Bind();
            
            GLuint program = m_shader.GetProgram();

            auto viewProjection = camera.GetViewProjectionMatrix();

            auto model = glm::mat4();
    
            auto shaderModel = glGetUniformLocation(program, "model");
            glUniformMatrix4fv(shaderModel, 1, GL_FALSE, glm::value_ptr(model));
            
            auto shaderViewProjection = glGetUniformLocation(program, "viewProjection");
            glUniformMatrix4fv(shaderViewProjection, 1, GL_FALSE, glm::value_ptr(viewProjection));
            
            glBindVertexArray(m_vao);
            glDrawArrays(GL_LINES, 0, m_elementCount);
            glBindVertexArray(0);
        }
    }
}