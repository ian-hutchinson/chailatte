set(headers ${headers} ChaiLatte)
set(headers ${headers} ChaiLatte/Include)
set(local_sources 
	ChaiLatte/main.cpp
	ChaiLatte/Types.h	
)

set(sources ${sources} ${local_sources})
source_group(\\ FILES ${local_sources})

include(ChaiLatte/Application/CMakeLists.txt)
include(ChaiLatte/Cameras/CMakeLists.txt)
include(ChaiLatte/Debug/CMakeLists.txt)
include(ChaiLatte/Images/CMakeLists.txt)
include(ChaiLatte/Inputs/CMakeLists.txt)
include(ChaiLatte/Lighting/CMakeLists.txt)
include(ChaiLatte/Logging/CMakeLists.txt)
include(ChaiLatte/Materials/CMakeLists.txt)
include(ChaiLatte/Modules/CMakeLists.txt)
include(ChaiLatte/Objects/CMakeLists.txt)
include(ChaiLatte/Rendering/CMakeLists.txt)
include(ChaiLatte/Scenes/CMakeLists.txt)
include(ChaiLatte/Shaders/CMakeLists.txt)
include(ChaiLatte/Textures/CMakeLists.txt)