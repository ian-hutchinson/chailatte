#include "Logger.h"
#include "App.h"

int main(int argc, const char* argv[])
{
    ChaiLatte::Logging::Logger::InitialiseLogFile("");
    
    ChaiLatte::Application::App *myApp = new ChaiLatte::Application::App();
    
    myApp->Run();
    
    delete myApp;
    ChaiLatte::Logging::Logger::Teardown();
    
    return 0;
}
