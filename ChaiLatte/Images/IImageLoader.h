//
//  IImageLoader.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 21/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_IImageLoader_h
#define ChaiLatte_IImageLoader_h

#include "Images.h"
#include "Types.h"
#include <string>

namespace ChaiLatte
{
    namespace Images
    {
        class IImageLoader
        {
        public:
            virtual ~IImageLoader(){ }
            virtual Image* LoadImage(const std::string& filename) = 0;
            virtual void FreeImage(Image* pImage) = 0;
        };
    }
}

#endif
