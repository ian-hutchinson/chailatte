//
//  Images.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 21/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Images_h
#define ChaiLatte_Images_h

namespace ChaiLatte
{
    namespace Images
    {
        class Image;
        class IImageLoader;
    }
}

#endif
