//
//  Image.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 21/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__Image__
#define __ChaiLatte__Image__

#include "Types.h"
#include "Images.h"
#include "ImageFormat.h"

namespace ChaiLatte
{
    namespace Images
    {
        class Image
        {
        public:
            Image(Byte* buffer, int width, int height, ImageFormat imageFormat, IImageLoader& imageLoader);
            ~Image();
            
            int GetWidth() const;
            int GetHeight() const;
            ImageFormat GetImageFormat() const;
            Byte* GetBuffer() const;
            
            void SetOriginalImage(void* pOriginalImage);
            void* GetOriginalImage();
            
        private:
            Byte* m_buffer;
            int m_width;
            int m_height;
            ImageFormat m_imageFormat;
            IImageLoader& m_imageLoader;
            
            void* m_pOriginalImage;
        };
    }
}

#endif /* defined(__ChaiLatte__Image__) */
