//
//  SoilImageLoader.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 21/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "SoilImageLoader.h"
#include "ImageFormat.h"
#include "Image.h"

#include <cassert>
#include <SOIL/SOIL.h>

namespace ChaiLatte
{
    namespace Images
    {
        SoilImageLoader::SoilImageLoader()
        {
            
        }
        
        SoilImageLoader::~SoilImageLoader()
        {
            
        }
        
        Image* SoilImageLoader::LoadImage(const std::string& filename)
        {
            int width;
            int height;
            int channels;

            Byte* imageData = SOIL_load_image(filename.c_str(), &width, &height, &channels, SOIL_LOAD_RGB);
            
            return new Image(imageData, width, height, ImageFormat::RGBA, *this);
        }
        
        void SoilImageLoader::FreeImage(Image* pImage)
        {
            SOIL_free_image_data(pImage->GetBuffer());
        }
    }
}