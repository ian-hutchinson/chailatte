//
//  Image.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 21/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "Image.h"
#include "IImageLoader.h"

namespace ChaiLatte
{
    namespace Images
    {
        Image::Image(Byte* buffer, int width, int height, ImageFormat imageFormat, IImageLoader& imageLoader)
        : m_buffer(buffer)
        , m_width(width)
        , m_height(height)
        , m_imageFormat(imageFormat)
        , m_imageLoader(imageLoader)
        {
            
        }
        
        Image::~Image()
        {
            m_imageLoader.FreeImage(this);
        }
        
        int Image::GetWidth() const
        {
            return m_width;
        }
        
        int Image::GetHeight() const
        {
            return m_height;
        }
        
        ImageFormat Image::GetImageFormat() const
        {
            return m_imageFormat;
        }
        
        Byte* Image::GetBuffer() const
        {
            return m_buffer;
        }
        
        void Image::SetOriginalImage(void *pOriginalImage)
        {
            m_pOriginalImage = pOriginalImage;
        }
        
        void* Image::GetOriginalImage()
        {
            return m_pOriginalImage;
        }
    }
}