//
//  ImageFormat.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 21/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_ImageFormat_h
#define ChaiLatte_ImageFormat_h

namespace ChaiLatte
{
    namespace Images
    {
        enum ImageFormat
        {
            RGB,
            RGBA,
            MAX_IMAGE_FORMAT
        };
    }
}
#endif
