//
//  SoilImageLoader.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 21/05/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__SoilImageLoader__
#define __ChaiLatte__SoilImageLoader__

#include "IImageLoader.h"

namespace ChaiLatte
{
    namespace Images
    {
        class SoilImageLoader : public IImageLoader
        {
        public:
            SoilImageLoader();
            ~SoilImageLoader();
            
            Image* LoadImage(const std::string& filename);
            void FreeImage(Image* pImage);
        };
    }
}

#endif /* defined(__ChaiLatte__SoilImageLoader__) */
