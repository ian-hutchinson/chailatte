//
//  IMoveableObject.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 24/03/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_IMoveableObject_h
#define ChaiLatte_IMoveableObject_h

#include "Transform.h"

namespace ChaiLatte
{
    namespace Objects
    {
        class IMoveableObject
        {
        public:
            virtual void SetTransform(Rendering::Transform& transform) = 0;
            virtual Rendering::Transform GetTransform() const = 0;
        };
    }
}

#endif
