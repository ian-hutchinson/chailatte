#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <assimp/scene.h>
#include <assimp/mesh.h>
#include "MeshEntry.h"
#include "IRenderable.h"
#include "Transform.h"
#include "Shaders.h"
#include "Textures.h"
#include "IMoveableObject.h"

#include <vector>
#include <string>

namespace ChaiLatte
{
    namespace Rendering
    {
        class Mesh
        {
        public:
            Mesh();
            ~Mesh();
            
            bool LoadMeshFromFile(const std::string& filename, bool swapYZ);
            void RenderMeshEntries();
        private:
            std::vector<MeshEntry*> m_meshEntries;
        };
    }
}


