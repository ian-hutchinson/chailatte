//
//  MeshEntry.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 12/02/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "MeshEntry.h"
#include "MeshHelpers.h"

namespace ChaiLatte
{
    namespace Rendering
    {
        MeshEntry::MeshEntry(aiMesh* pMesh, bool swapYZ)
        : m_elementCount(pMesh->mNumFaces * 3)
        {
            for (int i = 0; i < BufferSize; ++i)
            {
                m_vbo[i] = 0;
            }
            
            glGenVertexArrays(1, &m_vao);
            glBindVertexArray(m_vao);
            
            if (pMesh->HasPositions())
            {
                MeshHelpers::LoadVertices(pMesh, swapYZ, m_vbo[VertexBuffer]);
            }
            
            if (pMesh->HasTextureCoords(0))
            {
                MeshHelpers::LoadTextureCoordinates(pMesh, m_vbo[TexCoordBuffer]);
            }
            
            if (pMesh->HasNormals())
            {
                MeshHelpers::LoadNormals(pMesh, m_vbo[NormalBuffer]);
            }
            
            if (pMesh->HasFaces())
            {
                MeshHelpers::LoadIndices(pMesh, m_vbo[IndexBuffer]);
            }
            
            if (pMesh->HasTangentsAndBitangents())
            {
                MeshHelpers::LoadTangents(pMesh, m_vbo[TangentBuffer]);
            }
        
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
        }
        
        MeshEntry::~MeshEntry()
        {
            for (int i = 0; i < BufferSize; ++i)
            {
                if (m_vbo[i])
                {
                    glDeleteBuffers(1, &m_vbo[i]);
                }
            }
            
            glDeleteVertexArrays(1, &m_vao);
        }
        
        void MeshEntry::Render()
        {
            glBindVertexArray(m_vao);
            glDrawElements(GL_TRIANGLES, m_elementCount, GL_UNSIGNED_INT, NULL);
            glBindVertexArray(0);
        }
    }
}
