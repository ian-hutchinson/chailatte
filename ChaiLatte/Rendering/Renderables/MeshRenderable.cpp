//
// Created by Ian Hutchinson on 04/08/15.
//

#include "MeshRenderable.h"

namespace ChaiLatte
{
    namespace Rendering
    {
        namespace Renderables
        {
            MeshRenderable::MeshRenderable(Mesh& mesh, Materials::IMaterial& material)
            : m_mesh(mesh)
            , m_material(material)
            {

            }

            void MeshRenderable::Render(const Cameras::Camera& camera)
            {
                m_material.Prepare(*this, camera);

                m_mesh.RenderMeshEntries();

                m_material.CleanUp();
            }
        }
    }
}