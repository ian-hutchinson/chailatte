//
// Created by Ian Hutchinson on 04/08/15.
//

#ifndef CHAILATTE_RENDERABLEBASE_H
#define CHAILATTE_RENDERABLEBASE_H

#include "IRenderable.h"
#include "Transform.h"
#include "CamerasFwd.h"

namespace ChaiLatte
{
    namespace Rendering
    {
        namespace Renderables
        {
            class RenderableBase : public IRenderable
            {
            public:
                RenderableBase();

                Transform GetTransform() const;
                void SetTransform(const Transform& transform);
                glm::mat4 GetModel() const;

                virtual void Render(const Cameras::Camera& camera) = 0;

            private:
                Transform m_transform;
            };
        }
    }
}


#endif //CHAILATTE_RENDERABLEBASE_H
