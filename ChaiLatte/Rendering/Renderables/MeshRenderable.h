//
// Created by Ian Hutchinson on 04/08/15.
//

#ifndef CHAILATTE_MESHRENDERABLE_H
#define CHAILATTE_MESHRENDERABLE_H

#include "Materials/IMaterial.h"
#include "RenderableBase.h"
#include "Mesh.h"

namespace ChaiLatte
{
    namespace Rendering
    {
        namespace Renderables
        {
            class MeshRenderable : public RenderableBase
            {
            public:
                MeshRenderable(Mesh& mesh, Materials::IMaterial& material);
                void Render(const Cameras::Camera& camera);
            private:
                Mesh& m_mesh;
                Materials::IMaterial& m_material;

            };
        }
    }
}


#endif //CHAILATTE_MESHRENDERABLE_H
