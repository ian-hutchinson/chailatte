//
//  IRenderable.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 05/11/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_IRenderable_h
#define ChaiLatte_IRenderable_h

#include "Rendering.h"
#include "CamerasFwd.h"

namespace ChaiLatte
{
    namespace Rendering
    {
        namespace Renderables
        {
            class IRenderable
            {
            public:
                virtual ~IRenderable() { }
                virtual void Render(const Cameras::Camera& camera) = 0;
            };
        }
    }
}

#endif
