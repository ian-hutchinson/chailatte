//
// Created by Ian Hutchinson on 04/08/15.
//

#include "RenderableBase.h"
#include "Camera.h"

namespace ChaiLatte
{
    namespace Rendering
    {
        namespace Renderables
        {
            RenderableBase::RenderableBase()
            {

            }

            Transform RenderableBase::GetTransform() const
            {
                return m_transform;
            }

            void RenderableBase::SetTransform(const Transform& transform)
            {
                m_transform = transform;
            }

            glm::mat4 RenderableBase::GetModel() const
            {
                return m_transform.GetModel();
            }
        }
    }
}