//
//  VertexFlags.h
//  chailatte
//
//  Created by Ian Hutchinson on 26/10/2015.
//
//

#ifndef chailatte_VertexFlags_h
#define chailatte_VertexFlags_h

namespace ChaiLatte
{
    namespace Rendering
    {
        enum VertexFlags
        {
            None = 0,
            Position = (1 << 0),
            Normal = (1 << 1),
            UV = (1 << 2),
        };
    }
}

#endif
