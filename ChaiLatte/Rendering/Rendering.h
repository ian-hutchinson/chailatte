//
//  Rendering.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 08/09/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Rendering_h
#define ChaiLatte_Rendering_h

namespace ChaiLatte
{
    namespace Rendering
    {
        namespace Renderables
        {
            class IRenderable;
            class RenderableBase;
            class MeshRenderable;
        }
        class RenderQueue;
    }
}

#endif