//
//  Vertex.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 02/06/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Vertex_h
#define ChaiLatte_Vertex_h

#include <glm/glm.hpp>

namespace ChaiLatte
{
    namespace Rendering
    {
        struct PositionVertex
        {
            glm::vec3 position;
        };
        
        struct PositionNormalVertex
        {
            glm::vec3 position;
            glm::vec3 normal;
        };
        
        struct PositionNormalUvVertex
        {
            glm::vec3 position;
            glm::vec3 normal;
            glm::vec2 uv;
        };
        
        inline PositionVertex CreatePositionVertex(float x, float y, float z)
        {
            PositionVertex vertex;
            
            vertex.position = glm::vec3(x, y, z);
            
            return vertex;
        }
        
        inline PositionNormalVertex CreatePositionNormalVertex(float x, float y, float z,
                                                         float nx, float ny, float nz)
        {
            PositionNormalVertex vertex;
            
            vertex.position = glm::vec3(x, y, z);
            vertex.normal = glm::vec3(nx, ny, nz);
            
            return vertex;
        }
        
        inline PositionNormalUvVertex CreatePositionNormalUvVertex(float x, float y, float z,
                                                                   float nx, float ny, float nz,
                                                                   float u, float v)
        {
            PositionNormalUvVertex vertex;
            
            vertex.position = glm::vec3(x, y, z);
            vertex.normal = glm::vec3(nx, ny, nz);
            vertex.uv = glm::vec2(u, v);
            
            return vertex;
        }
    }
}

#endif
