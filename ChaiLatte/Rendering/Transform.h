//
//  Transform.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 14/10/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef ChaiLatte_Transform_h
#define ChaiLatte_Transform_h

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

namespace ChaiLatte
{
    namespace Rendering
    {
        struct Transform
        {
            Transform(const glm::vec3& position = glm::vec3(),
                      const glm::vec3& rotation = glm::vec3(),
                      const glm::vec3& scale = glm::vec3(1, 1, 1))
            : position(position)
            , rotation(rotation)
            , scale(scale)
            {
                
            }
            
            glm::vec3 position;
            glm::vec3 rotation;
            glm::vec3 scale;
            
            inline glm::mat4 GetModel() const
            {
                auto posMat = glm::translate(position);
                auto scaleMat = glm::scale(scale);
                auto rotX = glm::rotate(glm::radians(rotation.x), glm::vec3(1, 0, 0));
                auto rotY = glm::rotate(glm::radians(rotation.y), glm::vec3(0, 1, 0));
                auto rotZ = glm::rotate(glm::radians(rotation.z), glm::vec3(0, 0, 1));
                auto rotMat = rotX * rotY * rotZ;
                
                return posMat * rotMat * scaleMat;
            }
        };
    }
}

#endif
