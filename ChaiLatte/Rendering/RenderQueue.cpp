//
//  RenderQueue.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 08/09/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#include "RenderQueue.h"
#include "IRenderable.h"

namespace ChaiLatte
{
    namespace Rendering
    {
        RenderQueue::RenderQueue()
        : m_isEnqueuing(false)
        {
            
        }
        
        void RenderQueue::BeginEnqueuing()
        {
//            assert(!m_isEnqueuing);
//            m_renderables.clear();
            m_isEnqueuing = true;
        }
        
        void RenderQueue::Enqueue(Renderables::IRenderable &renderable)
        {
//            assert(m_isEnqueuing);
            m_renderables.push_back(&renderable);
        }
        
        void RenderQueue::EndEnqueuing()
        {
//            assert(m_isEnqueuing);
            m_isEnqueuing = false;
            
            // sort here
        }
        
        void RenderQueue::Render(const Cameras::Camera& camera)
        {
//            assert(!m_isEnqueuing);
            
            for (auto& renderable : m_renderables)
            {
                renderable->Render(camera);
            }
        }
    }
}