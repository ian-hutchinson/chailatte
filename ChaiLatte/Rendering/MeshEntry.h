#pragma once

#include "Transform.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <assimp/mesh.h>

#include <glm/glm.hpp>

#include <vector>


namespace ChaiLatte
{
    namespace Rendering
    {
        class MeshEntry
        {
        public:
            MeshEntry(aiMesh* pMesh, bool swapYZ);
            ~MeshEntry();
            
            void Render();
            
        private:
            enum Buffers
            {
                VertexBuffer,
                TexCoordBuffer,
                NormalBuffer,
                IndexBuffer,
                TangentBuffer,
                BufferSize
            };
            
            GLuint m_vao;
            GLuint m_vbo[BufferSize];
            unsigned int m_elementCount;
            
        };
    }
}