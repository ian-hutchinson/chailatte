//
//  Mesh.cpp
//  ChaiLatte
//
//  Created by Ian Hutchinson on 12/02/2015.
//  Copyright (c) 2015 Ian Hutchinson. All rights reserved.
//

#include "Mesh.h"
#include "Camera.h"
#include "IShader.h"
#include "ITexture.h"

#include <iostream> // for now, use better logger

#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <SOIL/SOIL.h>

#include <ctime>

namespace ChaiLatte
{
    namespace Rendering
    {
        Mesh::Mesh()
        {

        }
        
        Mesh::~Mesh()
        {
            for (auto entry : m_meshEntries)
            {
                delete entry;
            }
            
            m_meshEntries.clear();
        }
        
        bool Mesh::LoadMeshFromFile(const std::string &filename, bool swapYZ)
        {
            Assimp::Importer importer;
            const aiScene* pScene = importer.ReadFile(filename.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace);
            
            if (!pScene)
            {
                std::cerr << "Couldn't load model from " << filename << std::endl;
                return false;
            }
            
            for (int i = 0; i < pScene->mNumMeshes; ++i)
            {
                aiMesh* pMesh = pScene->mMeshes[i];
                m_meshEntries.push_back(new MeshEntry(pMesh, swapYZ));
            }

            return true;
        }

        void Mesh::RenderMeshEntries()
        {
            for (auto entry : m_meshEntries)
            {
                entry->Render();
            }
        }
    }
}