#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <assimp/mesh.h>

namespace ChaiLatte
{
    namespace Rendering
    {
        namespace MeshHelpers
        {
            void LoadVertices(aiMesh* pMesh, bool swapYZ, GLuint& out_vbo)
            {
                const unsigned int numVerts = pMesh->mNumVertices;
                float* verts = new float[numVerts * 3];
                
                for (int i = 0; i < numVerts; ++i)
                {
                    verts[i * 3] = pMesh->mVertices[i].x;
                    
                    if (swapYZ)
                    {
                        verts[i * 3 + 1] = pMesh->mVertices[i].z;
                        verts[i * 3 + 2] = pMesh->mVertices[i].y;
                    }
                    else
                    {
                        verts[i * 3 + 1] = pMesh->mVertices[i].y;
                        verts[i * 3 + 2] = pMesh->mVertices[i].z;
                    }
                }
                
                glGenBuffers(1, &out_vbo);
                glBindBuffer(GL_ARRAY_BUFFER, out_vbo);
                glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), verts, GL_STATIC_DRAW);
                
                glEnableVertexAttribArray(0);
                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                
                delete verts;
            }
            
            void LoadTextureCoordinates(aiMesh* pMesh, GLuint& out_vbo)
            {
                const unsigned int numVerts = pMesh->mNumVertices;
                float* texCoords = new float[numVerts * 2];
                
                for (int i = 0; i < numVerts; ++i)
                {
                    texCoords[i * 2] = pMesh->mTextureCoords[0][i].x;
                    texCoords[i * 2 + 1] = pMesh->mTextureCoords[0][i].y;
                }
                
                glGenBuffers(1, &out_vbo);
                glBindBuffer(GL_ARRAY_BUFFER, out_vbo);
                glBufferData(GL_ARRAY_BUFFER, 2 * numVerts * sizeof(GLfloat), texCoords, GL_STATIC_DRAW);
                
                glEnableVertexAttribArray(1);
                glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                
                delete texCoords;
            }
            
            void LoadNormals(aiMesh* pMesh, GLuint& out_vbo)
            {
                const unsigned int numVerts = pMesh->mNumVertices;
                float* normals = new float[numVerts * 3];
                
                for (int i = 0; i < numVerts; ++i)
                {
                    normals[i * 3] = pMesh->mNormals[i].x;
                    normals[i * 3 + 1] = pMesh->mNormals[i].y;
                    normals[i * 3 + 2] = pMesh->mNormals[i].z;
                }
                
                glGenBuffers(1, &out_vbo);
                glBindBuffer(GL_ARRAY_BUFFER, out_vbo);
                glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), normals, GL_STATIC_DRAW);
                
                glEnableVertexAttribArray(2);
                glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);

                delete normals;
            }
            
            void LoadIndices(aiMesh* pMesh, GLuint& out_vbo)
            {
                unsigned const int numIndices = pMesh->mNumFaces;
                unsigned int* indices = new unsigned int[numIndices * 3];
                
                for (int i = 0; i < numIndices; ++i)
                {
                    indices[i * 3] = pMesh->mFaces[i].mIndices[0];
                    indices[i * 3 + 1] = pMesh->mFaces[i].mIndices[1];
                    indices[i * 3 + 2] = pMesh->mFaces[i].mIndices[2];
                }
                
                glGenBuffers(1, &out_vbo);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, out_vbo);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * numIndices * sizeof(GLuint), indices, GL_STATIC_DRAW);
                
                delete indices;
            }

            
            void LoadTangents(aiMesh* pMesh, GLuint& out_vbo)
            {
                const unsigned int numVerts = pMesh->mNumVertices;
                float* tangents = new float[numVerts * 3];
                
                for (int i = 0; i < numVerts; ++i)
                {
                    tangents[i * 3] = pMesh->mTangents[i].x;
                    tangents[i * 3 + 1] = pMesh->mTangents[i].y;
                    tangents[i * 3 + 2] = pMesh->mTangents[i].z;
                }
                
                glGenBuffers(1, &out_vbo);
                glBindBuffer(GL_ARRAY_BUFFER, out_vbo);
                glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), tangents, GL_STATIC_DRAW);
                
                glEnableVertexAttribArray(3);
                glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                
                delete tangents;
            }
        }
    }
}