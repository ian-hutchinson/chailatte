//
//  RenderQueue.h
//  ChaiLatte
//
//  Created by Ian Hutchinson on 08/09/2014.
//  Copyright (c) 2014 Ian Hutchinson. All rights reserved.
//

#ifndef __ChaiLatte__RenderQueue__
#define __ChaiLatte__RenderQueue__

#include <vector>
#include <cassert>

#include "Rendering.h"
#include "CamerasFwd.h"

namespace ChaiLatte
{
    namespace Rendering
    {
        class RenderQueue
        {
        private:
            std::vector<Renderables::IRenderable*> m_renderables;
            bool m_isEnqueuing;
            
        public:
            RenderQueue();
            
            void Enqueue(Renderables::IRenderable & renderable);
            void BeginEnqueuing();
            void EndEnqueuing();
            void Render(const Cameras::Camera& camera);
        };
    }
}

#endif /* defined(__ChaiLatte__RenderQueue__) */
