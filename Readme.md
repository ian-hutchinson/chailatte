#ChaiLatte
ChaiLatte is a WIP OpenGL testbed. When looking at applicant's portfolio submissions, I often find that it's difficult to see a good, running example of OpenGL techniques. Even if a binary is supplied, it's kind of nice to see it with the code. In addition to this, the demos are often separate projects (MyOpenGLShadowMappingProject, MyOpenGLInstancingProject). Finally, the code in a lot of these examples can often be insular and crufty.

The goal of this project is to create an easy to view, easy to build and easy to run set of dynamic OpenGL examples built with well-engineered code.

#Building
You should generate this project using the cmake out-of-source build idiom. From the root of the repository, create a directory to contain your build. Then, from within that directory, generate the project using cmake.

```
mkdir XcodeBuild
cd XcodeBuild
cmake -G Xcode ..
```

#Visual Studio
Note, if you run from Visual Studio the application will be unable to find some of the relative paths. 
You need to set your debuggers working directory to chailatte/bin/<config> for your debug/release configuration.
To do this, open project properties and under Configuration Properties->Debugging set the Working Directory option.